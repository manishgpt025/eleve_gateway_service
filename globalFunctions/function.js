var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
const fs = require('fs');
module.exports = {
    /**
     * [Check post key validation]
     */
    checkRequest: (array, obj) => {
        for (i of array) {
            if (obj[i] == undefined || obj[i] == "")
                return i;
        }
        return true;
    },

    // uploading file to AWS
    uploadFile: async(userId, file, name, callback) => {
        let number = Math.random() * (999999 - 10000) + 10000
        let resData = {
            Key: userId + number + name,
            Body: file,
            contentType: 'images/jpeg',
            ACL: 'public-read'
        };
        await s3Bucket.upload(resData, (error, result) => {
            if (error) {
                return 0;
            } else {
                callback(null, result.Location);
            }
        });
    },

    // Upload image
    uploadFiles: async(userId, file,name, callback) => {
        let number = Math.random() * (999999 - 10000) + 10000;
         fs.readFile(file, async function (err, image) {
            let resData = {
                Key: userId + number + name,
                Body: image,
               'ContentType' : 'image/jpeg',
                ACL: 'public-read'
            };
            await s3Bucket.upload(resData, (error, result) => {
                if (error) {
                    return 0;
                } else {
                    callback(null, result.Location);
                }
            });
        });

    },

    // Upload video
    uploadVideoFile: async(userId, file,name, callback) => {
        let number = Math.random() * (999999 - 10000) + 10000;
        fs.readFile(file, async function (err, video) {
            let resData = {
                Key: userId + number + name,
                Body: video,
                'ContentType': 'video/mp4',
                ACL: 'public-read'
            };
            await s3Bucket.upload(resData, (error, result) => {
                if (error) {
                    return 0;
                } else {
                    callback(null, result.Location);
                }
            });
        });

    },

       // Upload pdf
       uploadPdfFiles: async(userId, file,name, callback) => {
        let number = Math.random() * (999999 - 10000) + 10000;
         fs.readFile(file, async function (err, image) {
            let resData = {
                Key: userId + number + name,
                Body: image,
               'ContentType' : 'application/pdf',
                ACL: 'public-read'
            };
            await s3Bucket.upload(resData, (error, result) => {
                if (error) {
                    return 0;
                } else {
                    callback(null, result.Location);
                }
            });
        });

    },
    errorResponse : (error) => {
        let errorObject = {};
        if(error.responseCode){
             errorObject.responseCode = error.responseCode;
        }
        if(error.statusCode){
             errorObject.statusCode = error.statusCode;
        }
        if(error.responseMessage){
            errorObject.responseMessage = error.responseMessage;
       }
       if(error.error && error.error.message){
            errorObject.responseMessage = error.error.message;
       }
       errorObject.result = {};
        return errorObject;
    }

}