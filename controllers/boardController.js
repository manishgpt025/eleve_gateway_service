//Pakages
const request = require('request-promise');
const board_base_url = global.gConfig.board_base_url;
//use helper
const helper = require('../globalFunctions/function.js');
let jwt = require('jsonwebtoken');
const secret = `${global.gConfig.secret}`;

// CSV to JSON
const csv = require('csvtojson');
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const aws_config = AWS.config.loadFromPath('./s3_config.json');

//  {accessKeyId: config.accessKeyId,secretAccessKey: config.secretAccessKey,region: config.region}



/**
 * [Health Check]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const healthCheck = (req, res) => {


    var healthPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'GET',
            uri: board_base_url + `/api/health-check`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    healthPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/** //Updated
 * [Board List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const boardList = (req, res) => {

    req.query.decoded = req.body.decoded;

    var listPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'GET',
            uri: board_base_url + `/api/boards/list`,
            json: req.query,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    listPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/**
 * [login brand of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const startBoard = (req, res) => {
    var startBoardPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'GET',
            uri: board_base_url + `/api/boards/start`,
            json: req.body,
            resolveWithFullResponse: true
        }
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    startBoardPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/**
 * [login brand of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getBoard = (req, res) => {

    var getBoardPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'GET',
            uri: board_base_url + `/api/boards/` + req.params.boardId,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getBoardPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })

}



/**
 * [login brand of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateBoard = (req, res) => {
    var updateBoardPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            //uri: board_base_url + `/api/boards/` + req.params.boardId,
            uri: board_base_url + `/api/boards/create`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    updateBoardPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

//Delete Board
const deleteBoard = (req, res) => {
    var deleteBoardPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'PATCH',
            uri: board_base_url + `/api/boards/` + req.params.boardId,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    deleteBoardPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/**
 * [board colleagues]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const uploadBoardContents = async(req, res) => {
    const multiparty = require('multiparty');
    const csvData = new multiparty.Form();
    csvData.parse(req, function(err, fields, files) {
        var csvFilePath = files.file[0].path;
        csv()
            .fromFile(csvFilePath)
            .then((jsonObj) => {
                console.log(jsonObj);
                var dataArray = {
                    "board_id": fields.board_id,
                    "data": jsonObj
                }
                const options = {
                    uri: board_base_url + `/api/coll/contentsUpload`,
                    json: dataArray,
                    resolveWithFullResponse: true,
                    method: 'POST'
                };
                return request(options).then((response) => {
                    res.send(response.body);
                }).catch((err) => {
                    console.log(err);
                    console.log('errorstatuscode:' + err.statusCode)
                });
            });

    });
}

const boardColleagues = (req, res) => {
    var boardColleaguesPromise = new Promise(function(resolve, reject) {
        req.body.adverserData = req.decoded;
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/coleagues`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    boardColleaguesPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

const s3Config = new AWS.S3(aws_config);

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

const multerS3Config = multerS3({
    s3: s3Config,
    bucket: process.env.S3_BUCKET_NAME,
    metadata: function(req, file, cb) {
        cb(null, { fieldName: file.fieldname });
    },
    acl: 'public-read',
    key: function(req, file, cb) {
        var newFileName = Date.now() + "-" + file.originalname;
        var fullPath = 'collaboration_file/' + newFileName;
        cb(null, fullPath);
    }
});

const uploadFiles = multer({
    storage: multerS3Config,
    limits: {
        fileSize: 1024 * 1024 * 5 // we are allowing only 5 MB files
    }
})

// const multerS3Config1 = multerS3({
//     s3: s3Config,
//     bucket: process.env.S3_BUCKET_NAME,
//     metadata: function(req, file, cb, board_id) {
//         console.log(req.body,'--------')
//         cb(null, { fieldName: file.fieldname });
//     },
//     acl: 'public-read',
//     key: function(req, file, cb, board_id) {
//         var newFileName = Date.now() + "-" + file.originalname;
//         var fullPath = 'collaboration_file/' + newFileName;
//         cb(null, fullPath);
//     },
// });

// const uploadFile = multer({
//     storage: multerS3Config1,
//     // fileFilter: fileFilter,
//     limits: {
//         fileSize: 1024 * 1024 * 5 // we are allowing only 5 MB files
//     }
// })

/*Update Report in Board Details */
const updateReport = (req, res) => {
    var updateReportPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/update`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    updateReportPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}


function saveRo(req, res) {


    var saveRoPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/savero`,
            json: { "file_path": req.file_path, "board_id": req.board_id },
            resolveWithFullResponse: true
        }
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(true);
                } else {
                    reject(false);
                }

            })
            .catch(e => {
                return false;
            })
    })

    // saveRoPromise.then(response => {

    //             return true;
    //     })
    //     .catch(e => {
    //          return false;
    //     })
    return saveRoPromise;
}


/* Remove Board Member */
const removeBoardMember = (req, res) => {
    var removeMemberPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/removemember`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    removeMemberPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/* Remove Board Atatchment */
const removeBoardAttachment = (req, res) => {
    var removeAttachmentPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/removeattachment`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    removeAttachmentPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/*Content of Board Submit */
const submitContentBoard = (req, res) => {
    req.body.decoded = req.decoded;
    var submitContentPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/contentsubmit`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    submitContentPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}


/*Board content Confirm Action */
const boardContentConfirm = (req, res) => {
    var contentConfirmPromise = new Promise(function(resolve, reject) {
        req.body.decoded = req.decoded;
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/contentconfirm`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    contentConfirmPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/*Board content List*/
const boardContentList = (req, res) => {

    var contentListPromise = new Promise(function(resolve, reject) {
        req.body.decoded = req.decoded;

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/contentlist`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    contentListPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}


/*Board content Search Influencer List*/
const boardContentSearchInfluencer = (req, res) => {

    var searchInfluencerPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/searchinfluencer`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchInfluencerPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/* Board Infulencers Contracts */
const getEcontracts = (req, res) => {

    req.query.decoded = req.decoded;
    var getInfluencerEcontracts = new Promise(function(resolve, reject) {

        const options = {
            method: 'GET',
            uri: board_base_url + `/api/boards/influencer/econtracts`,
            json: req.query,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getInfluencerEcontracts.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}



// API to get the latest econtract of the influencer on board
const getLatestEcontract = (req, res) => {

    req.body.decoded = req.decoded;
    var getEcontractsPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/latestEcontract`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })
    getEcontractsPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/** Add social profile of influencer */
const addSocial = (req, res) => {

    var addSocialPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + '/api/boards/addSocial',
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    addSocialPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/** get Overview Data of Published Tab */
const getOverviewData = (req, res) => {
    var getData = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + '/api/coll/conversationoveriew',
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
            if (response.body.responseCode == 200) {
                res.send(response.body);
            } else {
                res.send(response.body);
            }
        }).catch((err) => {
            console.log('error_status_code:' + err.statusCode);
        });
    })
    getData.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}


/*getOverviewList*/
const getOverviewList = (req, res) => {
    var getInfluencerlist = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/conversationinfluencerlist`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    //res.send(response.body);
                    resolve(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })
    getInfluencerlist.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

// Defining the type of file
const filterEcontracts = (req, file, cb) => {
    if (file.mimetype === 'application/pdf') {
        cb(null, true)
    } else {
        cb(null, false)
    }
}


//Save e-contracts
const saveEcontracts = (req, res) => {

    req.body.decoded = req.decoded;
    var saveContracts = new Promise(function(resolve, reject) {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/saveEcontracts`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
            if (response.body.responseCode === 200) {
                resolve(response.body);
            } else {
                reject(response.body);
            }
        })
    });

    saveContracts.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(e);
        })
}


// remove influencer from the board on influencer Tab
const removeKol = (req, res) => {
    var removeKolPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/removeKol`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    removeKolPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

// remove social profile from the board on influencer Tab
const removeSocial = (req, res) => {

    var removeKolPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/removeSocial`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    removeKolPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

// Add notes for the influencer
const addNotes = (req, res) => {
    var addNotesPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/addNotes`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    addNotesPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

// Add brief for the influencer
const addBrief = (req, res) => {

    req.body.decoded = req.decoded;
    var addBriefPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/addBrief`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {

                res.send(helper.errorResponse(e));
            })
    })

    addBriefPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/*Search Influencer in Influencers Tab */
const getSearchInfluencer = (req, res) => {

    var searchInfluencerPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/searchinfluencertab`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {

                res.send(helper.errorResponse(e));
            })
    })

    searchInfluencerPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/*Add Influencer in Influencer Tab */
const addInfluencerTab = (req, res) => {

    var addInfluencerPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/add/influencer`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {

                res.send(helper.errorResponse(e));
            })
    })

    addInfluencerPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}


/*Update Status in Influencers Tab */
const updateStatus = (req, res) => {

    var updateStatusPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/changestatus`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {

                res.send(helper.errorResponse(e));
            })
    })

    updateStatusPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/*Invite Influencer */

const inviteInfluencer = (req, res) => {

    var inviteInfluencerPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/inviteinfluencer`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {

                res.send(helper.errorResponse(e));
            })
    })

    inviteInfluencerPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

//get board Platforms
const getboardPlatforms = (req, res) => {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/boardplatforms`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
            if (response.body) {
                res.send(response.body);
            } else {
                res.send({ 'responseCode': 404, 'responseMessage': 'error' });
            }

        }).catch((err) => {
            res.send({ 'responseCode': 500, 'responseMessage': 'error' });

        })
    }
    // board status List
const statusList = (req, res) => {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/statuslist`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
            if (response.body) {
                res.send(response.body);
            } else {
                res.send({ 'responseCode': 404, 'responseMessage': 'error' });
            }

        }).catch((err) => {
            res.send({ 'responseCode': 500, 'responseMessage': 'error' });

        })
    }
    //board estimated List
const estimatedList = (req, res) => {
    const options = {
        method: 'POST',
        uri: board_base_url + `/api/coll/estimatedlist`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body == 200) {
            res.send(response.body);
        } else {
            res.send(response.body);
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

const getBoardInfluencers = (req, res) => {

        req.query.user_token = req.decoded;
        var getBoardInfluencers = new Promise(function(resolve, reject) {

            const options = {
                method: 'GET',
                uri: board_base_url + `/api/boards/influencer/list`,
                json: req.query,
                resolveWithFullResponse: true,
            };
            return request(options).then((response) => {
                    if (response.body.responseCode == 200) {
                        resolve(response.body);
                    }
                    reject(response.body);

                })
                .catch(e => {
                    res.send(helper.errorResponse(e));
                })
        })

        getBoardInfluencers.then(response => {
                res.send(response);
            })
            .catch(e => {
                console.log("coming");
                res.send(helper.errorResponse(e));
            })

    }
    // multiple Invite Influencer
const multipleInviteInfluencer = (req, res) => {
    const options = {
        method: 'POST',
        uri: board_base_url + `/api/coll/multipleinvite`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body == 200) {
            res.send(response.body);
        } else {
            res.send(response.body);
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

const rejectBrief = (req, res) => {

    var briefPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/influencer/rejectBrief`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    briefPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            console.log("coming");
            res.send(helper.errorResponse(e));
        })

}
/**Influencer content submit */
const influencerContentSubmit = (req, res) => {

    var influencerSubmit = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/influencercoll/contentsubmit`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    influencerSubmit.then(response => {
            res.send(response);
        })
        .catch(e => {
            console.log("coming");
            res.send(helper.errorResponse(e));
        })

}

/**Influencer content list */
const influencerContentList = (req, res) => {

    var contentList = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/influencercoll/contentlist`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    contentList.then(response => {
            res.send(response);
        })
        .catch(e => {
            console.log("coming");
            res.send(helper.errorResponse(e));
        })

}

/**Influencer Selected Profiles */
const getSelectedProfiles = (req, res) => {

    var selectedProfile = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/influencercoll/socialprofiles`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    selectedProfile.then(response => {
            res.send(response);
        })
        .catch(e => {
            console.log("coming");
            res.send(helper.errorResponse(e));
        })

}

/* get board details of a influncer */
const getBoardDetails = (req, res) => {
    req.query.decoded = req.body.decoded;
    var getBoardDetails = new Promise(function(resolve, reject) {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/influencercoll/get-board-details`,
            json: req.query,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {

                res.send(helper.errorResponse(e));
            })
    })

    getBoardDetails.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })

}

/* get board details of a influncer */
const getBriefDetails = (req, res) => {

    var getBoardDetails = new Promise(function(resolve, reject) {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/getbriefdetails`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getBoardDetails.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })

}

const searchBrandColleagues = (req, res) => {
    var searchBrandColleagues = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/searchorganisationcolleagues`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchBrandColleagues.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })

}

// search Brand
const searchBrand = (req, res) => {
    req.body.decoded = req.decoded;
    var searchBrand = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/searchbrand`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })


    searchBrand.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

/* For saving actions (accept and reject )of an influencer in a board*/
const saveInfluencerAction = async (req,res) => {
    try {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/influencercoll/save-influencer-action`,
            json: req.body,
            resolveWithFullResponse: true,

        };

        let response = await request(options);
        res.send(response.body);

    } catch (err) {
        res.send(helper.errorResponse(err));
    }
}
/*get recent update  */
const getRecentUpdates = async (req,res) => {

    try {
          const options = {
              method: 'POST',
              uri: board_base_url + `/api/influencercoll/recentupdate`,
              json: req.body,
              resolveWithFullResponse: true,

          };

          let response =  await request(options);
          res.send(response.body);

    } catch(err){
       res.send(helper.errorResponse(err));
    }
  }

  /*get recent update  */
const getBoardList = async (req,res) => {

    try {
          const options = {
              method: 'POST',
              uri: board_base_url + `/api/influencercoll/activeboardList`,
              json: req.body,
              resolveWithFullResponse: true,

          };

          let response =  await request(options);
          res.send(response.body);

    } catch(err){
       res.send(helper.errorResponse(err));
    }
  }
// Export
module.exports = {
    healthCheck: healthCheck,
    boardList: boardList,
    startBoard: startBoard,
    getBoard: getBoard,
    updateBoard: updateBoard,
    uploadBoardContents,
    boardColleagues: boardColleagues,
    uploadFiles,
    deleteBoard: deleteBoard,
    //uploadRo,
    saveRo,
    removeBoardMember: removeBoardMember,
    updateReport,
    removeBoardAttachment,
    submitContentBoard,
    boardContentConfirm,
    boardContentList,
    boardContentSearchInfluencer,
    getEcontracts,
    addSocial,
    getLatestEcontract,
    saveEcontracts,
    getOverviewData,
    getOverviewList,
    //uploadFile,
    removeKol,
    addNotes,
    addBrief,
    getSearchInfluencer,
    addInfluencerTab,
    updateStatus,
    inviteInfluencer,
    getBoardInfluencers,
    getboardPlatforms,
    statusList,
    rejectBrief,
    estimatedList,
    multipleInviteInfluencer,
    influencerContentSubmit,
    influencerContentList,
    getSelectedProfiles,
    getBoardDetails,
    removeSocial,
    getBriefDetails,
    searchBrandColleagues,
    searchBrand,
    saveInfluencerAction,
    getRecentUpdates,
    getBoardList
}