// Packages
const request = require('request-promise');
const multiparty = require('multiparty');
const fs = require('fs');
var Twitter = require('twitter');
var FormData = require('form-data');
// Configurations
const userbaseUrl = global.gConfig.base_url;
const campaignbaseUrl = global.gConfig.campaign_base_url;
// Helpers
const helper = require('../globalFunctions/function.js');


/**
 * [created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const createInfluencer = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/create`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [check email from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkEmailExist = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/checkEmail`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [edit deatil for influencer from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const editDetailInfluencer = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/editDetailInfluencer`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [get deatil by influencer id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getInfluencerDetail = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/detail`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [get deatil by influencer id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const editSocialDetailInfluencer = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/editSocialDetailInfluencer`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [status Update of influencer  on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const statusUpdate = (req, res) => {
    const options = {
        uri: userbaseUrl + `/api/influencer/status`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Get all influencers on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getInfluencerList = (req, res) => {
        console.log(req.body)
        const options = {
            uri: userbaseUrl + `/api/influencer/list`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
            if (response.body) {
                res.send(response.body);
            } else {
                res.send({ 'responseCode': 404, 'responseMessage': 'error' });
            }
        }).catch((err) => {
            res.send({ 'responseCode': 500, 'responseMessage': 'error' });
        })
    }
    /**
     * [Get all getInfluencerListDataForExcel on Admin Section]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */
const getInfluencerListDataForExcel = (req, res) => {
    console.log(req.body)
    const options = {
        uri: userbaseUrl + `/api/influencer/getInfluencerListDataForExcel`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Get all influencers on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const deleteSocialCard = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/deleteSocialCard`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [genre List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const genreList = (req, res) => {
        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/influencer/genreList`,
            json: req.body,
            resolveWithFullResponse: true,

        };
        return request(options).then((response) => {
            if (response.body) {
                res.send(response.body);
            } else {
                res.send({ 'responseCode': 404, 'responseMessage': 'error' });
            }
        }).catch((err) => {
            res.send({ 'responseCode': 500, 'responseMessage': 'error' });
        })
    }
    /**
     * [check Handle Url]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */
const checkHandleUrl = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/checkHandleUrl`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/******************** Influencer Campaign Management ********************/
/**
 * [Influencer Campaign Management]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignNew = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/campaign/new`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Influencer Campaign Details]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignDetail = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/campaign/detail`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Influencer Campaign Accepted Management]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignCompleted = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/campaign/completed`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

const campaignAction = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/actioncampaign`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Influencer Campaign Twitter Post]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignTwitterPost = (req, res) => {

    if (req.body.file) {
        var posted_data = {
            campaign_influencer_id: req.body.campaign_influencer_id,
            file: req.body.file,
            text: req.body.text,
            oauth_token: req.body.oauth_token,
            oauth_token_secret: req.body.oauth_token_secret,
            decoded : req.decoded
        }
    } else {
        var posted_data = {
            campaign_influencer_id: req.body.campaign_influencer_id,
            text: req.body.text,
            oauth_token: req.body.oauth_token,
            oauth_token_secret: req.body.oauth_token_secret,
            decoded : req.decoded
        }
    }
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/campaign/twitter/post`,
        json: posted_data,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        console.log(response);
        res.send(response.body);
    }).catch((err) => {
        console.log(err);
        console.log('errorstatuscode:' + err.statusCode)
    })
}


/**
 * [Influencercheck Authentication]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkAuthentication = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/checkauthentication`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Get all social platforms of a influencer]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getPlatforms = (req, res) => {
    const options = {
        method: 'POST',
        uri: userbaseUrl + '/api/influencer/get_all_platforms',
        json: req.body,
        resolveWithFullResponse: true
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Update Retweet Count]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const retweetUpdateCount = (req, res) => {
        const options = {
            method: 'POST',
            uri: userbaseUrl + '/api/influencer/reweetcount',
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
            if (response.body == 200) {
                res.send(response.body);
            } else {
                res.send(response.body);
            }
        }).catch((err) => {
            res.send({ 'responseCode': 500, 'responseMessage': 'error' });
        })
    }

    /**
 * [created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const get_max_reach_count = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        method: 'POST',
        uri: userbaseUrl + `/api/influencer/getMaxReachCountData`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'Not found' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'Somethings went wrong' });
    })
}
    /******************** Influencer Campaign Management Ends ********************/

// Export
module.exports = {
    createInfluencer,
    checkEmailExist,
    editDetailInfluencer,
    getInfluencerDetail,
    statusUpdate,
    getInfluencerList,
    editSocialDetailInfluencer,
    deleteSocialCard,
    genreList,
    checkHandleUrl,
    campaignNew,
    campaignDetail,
    campaignCompleted,
    campaignAction,
    getInfluencerListDataForExcel,
    campaignTwitterPost,
    checkAuthentication,
    getPlatforms,
    retweetUpdateCount,
    get_max_reach_count
}