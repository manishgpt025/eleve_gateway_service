const request = require('request-promise');
const brandBaseUrl = global.gConfig.brand_base_url;
const board_base_url = global.gConfig.board_base_url;
//use helper
const helper = require('../globalFunctions/function.js');
let jwt = require('jsonwebtoken');
const secret = `${global.gConfig.secret}`;
/**
 * [login brand of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const brandLogin = (req, res) => {
    var getBoardDetails = new Promise(function(resolve, reject) {
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/boards/brand/login`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    let token = jwt.sign({ email: req.body.email, type: 3 },
                        secret, {
                            expiresIn: '24h' // expires in 24 hours
                        }
                    );
                    response.body.token = token;
                    resolve(response.body);
                }
                reject(response.body);

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getBoardDetails.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })

}

// Export
module.exports = {
    brandLogin
}