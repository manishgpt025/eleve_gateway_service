//Pakages
const request = require('request-promise');
const board_base_url = global.gConfig.board_base_url;
//use helper
const helper = require('../globalFunctions/function.js');
// CSV to JSON
const csv = require('csvtojson');
const multipartyData = require('multiparty');


/**
 * [youtube Upload Video]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const youtubeUploadVideo = async(req, res) => {
    let youtubeUpload = new Promise(function(resolve, reject) {
        let csvData = new multipartyData.Form();
        csvData.parse(req, function(err, fields, files) {
            if(files && files.file_data){
                let csvFilePath = files.file_data[0].path;
                csv()
                    .fromFile(csvFilePath)
                    .then((jsonObj) => {
                        let dataArray = {
                            "admin_data": req.decoded,
                            "csv_data": jsonObj
                        }
                        const options = {
                            uri: board_base_url + `/api/coll/youtubeIdUpload`,
                            json: dataArray,
                            resolveWithFullResponse: true,
                            method: 'POST'
                        };
                        return request(options).then((response) => {
                            if (response.body.responseCode == 200) {
                                resolve(response.body);
                            } else {
                                reject(response.body);
                            }
                        })
                        .catch(e => {
                            res.send(helper.errorResponse(e));
                        })
                    });
            }else{
                res.send('File not found.');
            }

        });
    })
    youtubeUpload.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [upload Youtube Channels]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const uploadYoutubeChannel = async(req, res) => {
    let youtubeUpload = new Promise(function(resolve, reject) {
        let csvData = new multipartyData.Form();
        csvData.parse(req, function(err, fields, files) {
            if(files && files.file_data){
                let csvFilePath = files.file_data[0].path;
                csv()
                    .fromFile(csvFilePath)
                    .then((jsonObj) => {
                        let dataArray = {
                            "admin_data": req.decoded,
                            "csv_data": jsonObj
                        }
                        const options = {
                            uri: board_base_url + `/api/coll/youtubeChannelIdUpload`,
                            json: dataArray,
                            resolveWithFullResponse: true,
                            method: 'POST'
                        };
                        return request(options).then((response) => {
                            if (response.body.responseCode == 200) {
                                resolve(response.body);
                            } else {
                                reject(response.body);
                            }
                        })
                        .catch(e => {
                            res.send(helper.errorResponse(e));
                        })
                    });
            }else{
                res.send('File not found.');
            }

        });
    })
    youtubeUpload.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [get Channels Data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getChannelsData = (req, res) => {
    var adverListPromise = new Promise(function(resolve, reject) {
        req.body.admin_data = req.decoded
        const options = {
            method: 'POST',
            uri: board_base_url + `/api/coll/getChannelsData`,
            json: req.body,
            resolveWithFullResponse: true,

        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })
    adverListPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}
// Export
module.exports = {
    youtubeUploadVideo,
    uploadYoutubeChannel,
    getChannelsData
}