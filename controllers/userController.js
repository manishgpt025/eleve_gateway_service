const request = require('request-promise');
const baseUrl = global.gConfig.base_url;
// const config = require('../config.js');
const secret = `${global.gConfig.secret}`;
// excelToJson
const excelToJson = require('convert-excel-to-json');
// CSV to JSON
const csv = require('csvtojson');
// JWT
let jwt = require('jsonwebtoken');
var array = [];
//use helper
const helper = require('../globalFunctions/function.js');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
// var AWS = require('aws-sdk');
// AWS.config.loadFromPath('./s3_config.json');
// var s3Bucket = new AWS.S3({ params: { Bucket: 'eleve-global' } });
/**
 * [createUser new user to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const register = (req, res) => {
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/postregister`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {

        if (response.body.responseCode == 200) {
            // console.log('Response ==> ', response.body);

            // JWT
            let token = jwt.sign({ email: req.body.email, type: 2 },
                secret, {
                    expiresIn: '24h' // expires in 24 hours
                }
            );

            response.body.token = token;
            // console.log('token ==> ', token);
            res.send(response.body);
        } else {
            res.send(response.body);
        }
    }).catch((err) => {
        console.log(err);
        console.log('errorstatuscode:' + err.statusCode)
    })
}

/**
 * [createUser new subscribe of a user to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const subscribe = (req, res) => {
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/postsubscribe`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        console.log(err);
        console.log('errorstatuscode:' + err.statusCode)
    })
}

/**
 * [user to be login from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const login = (req, res) => {
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/login`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {

        if (response.body.responseCode == 200) {
            // console.log('Response ==> ', response.body);

            // JWT
            let token = jwt.sign({ email: req.body.email, type: 2 },
                secret, {
                    expiresIn: '24h' // expires in 24 hours
                }
            );

            response.body.token = token;
            // console.log('token ==> ', token);
            res.send(response.body);
        } else {
            res.send(response.body);
        }
    }).catch((err) => {
        console.log(err);
        console.log('errorstatuscode:' + err.statusCode)
    })
}

/**
 * [User Logout]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const logout = (req, res) => {
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/logout`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [User fb auth]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const facebookAuth = (req, res) => {
    const options = {
        method: 'GET',
        uri: baseUrl + `/api/facebook/auth`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [FB callback]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const facebookCallback = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/facebook/callback`,
        json: req.body,
        resolveWithFullResponse: true,
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [forgot password user to be forgot from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const forgotPassFunction = (req, res) => {

    const options = {
        uri: baseUrl + `/api/forgot-password/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [reset password user to be reset from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const resetFunction = (req, res) => {

    const options = {
        uri: baseUrl + `/api/reset-password/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [instagram handle user to be login from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const instahandleauth = (req, res) => {
    // console.log(req.body);
    // return false;
    const options = {
        uri: baseUrl + `/api/handleauth/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [check link expire  to be expire from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const checkExpiryFunction = (req, res) => {
    console.log(req.body);

    const options = {
        uri: baseUrl + `/api/check-expiry/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [instagram login by api on social page]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const instagramLogin = (req, res) => {

    const options = {
        uri: baseUrl + `/api/instagram/`,
        json: true,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [instagramCallBack  by api on social page for user data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const instagramCallBack = (req, res) => {

    const options = {
        uri: baseUrl + `/api/instagram-callback/`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [change password user  to be changed from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const changepassword = (req, res) => {
    console.log(req.body);

    const options = {
        uri: baseUrl + `/api/change/password/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'PUT'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [check password user to be checked from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const currentpassword = (req, res) => {
    const options = {
        uri: baseUrl + `/api/checkpassword/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [save user skills]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addUserSkills = (req, res) => {
    const options = {
        uri: baseUrl + `/api/skills/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'PUT'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Get user data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getUserData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Get user data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateUserData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'PUT'
    }
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [Get user bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getUserBankData = (req, res) => {
    console.log(req.body);
    const options = {
        uri: baseUrl + `/api/user/bank`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [check the email for verification]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const checkemailverification = (req, res) => {
    const options = {
        uri: baseUrl + `/api/checkemail`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    }
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Edit user bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const editUserBankData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user/bank`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'PUT'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Email Verify]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const emailverify = (req, res) => {
    const options = {
        uri: baseUrl + `/api/emailverify`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    }
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Delete user bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const deleteUserBankData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user/bank`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'DELETE'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Get user payment data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getUserPaymentData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/payment_gst`,
        // json: req.body,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Send Email Link for Verification]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const email_send_link = (req, res) => {
    const options = {
        uri: baseUrl + `/api/emailsendlink`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    }
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [Add user payment data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addUserPaymentData = (req, res) => {

    const multiparty = require('multiparty');
    const paymentForm = new multiparty.Form();
    paymentForm.parse(req, function(err, fields, files) {
        console.log('field----------------->', fields);
        console.log('files----------------->', files);
        // Upload Images
        if (fields.country_check[0] == "India") {
            if (files.pan_upload) {
                helper.uploadPdfFiles(fields.user_id, files.pan_upload[0].path, files.pan_upload[0].originalFilename, (error, panUpload) => {
                    if (error) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        if (files.gst_upload) {
                            helper.uploadPdfFiles(fields.user_id, files.gst_upload[0].path, files.gst_upload[0].originalFilename, (error1, gstUpload) => {
                                if (error1) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);
                                } else {
                                    let dataArray = {
                                        country_name: fields.country_check ? fields.country_check[0] : '',
                                        state_name: fields.state_name ? fields.state_name[0] : '',
                                        city_name: fields.city_name ? fields.city_name[0] : '',
                                        billing_address: fields.billing_address ? fields.billing_address[0] : '',
                                        pan_number: fields.pan_number ? fields.pan_number[0] : '',
                                        gst: fields.gst ? fields.gst[0] : '',
                                        gst_number: fields.gst_number ? fields.gst_number[0] : '',
                                        sac_code: fields.sac_code ? fields.sac_code[0] : '',
                                        nature_service: fields.nature_service ? fields.nature_service[0] : '',
                                        state_code: fields.state_code ? fields.state_code[0] : '',
                                        ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                                        npwp: fields.npwp ? fields.npwp[0] : '',
                                        tax_id: fields.tax_id ? fields.tax_id[0] : '',
                                        business_relation: fields.business_relation ? fields.business_relation[0] : '',
                                        user_id: fields.user_id ? fields.user_id[0] : '',
                                        pan_image: panUpload,
                                        gst_image: gstUpload
                                    }
                                    const options = {
                                        uri: baseUrl + `/api/payment_add`,
                                        json: dataArray,
                                        resolveWithFullResponse: true,
                                        method: 'POST'
                                    };
                                    return request(options).then((response) => {
                                        console.log(response);
                                        res.send(response.body);
                                    }).catch((err) => {
                                        console.log(err);
                                        console.log('errorstatuscode:' + err.statusCode)
                                    })

                                }
                            });
                        } else {
                            let dataArray = {
                                country_name: fields.country_check ? fields.country_check[0] : '',
                                state_name: fields.state_name ? fields.state_name[0] : '',
                                city_name: fields.city_name ? fields.city_name[0] : '',
                                billing_address: fields.billing_address ? fields.billing_address[0] : '',
                                pan_number: fields.pan_number ? fields.pan_number[0] : '',
                                gst: fields.gst ? fields.gst[0] : '',
                                gst_number: fields.gst_number ? fields.gst_number[0] : '',
                                sac_code: fields.sac_code ? fields.sac_code[0] : '',
                                nature_service: fields.nature_service ? fields.nature_service[0] : '',
                                state_code: fields.state_code ? fields.state_code[0] : '',
                                ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                                npwp: fields.npwp ? fields.npwp[0] : '',
                                tax_id: fields.tax_id ? fields.tax_id[0] : '',
                                business_relation: fields.business_relation ? fields.business_relation[0] : '',
                                user_id: fields.user_id ? fields.user_id[0] : '',
                                pan_image: panUpload
                            }
                            const options = {
                                uri: baseUrl + `/api/payment_add`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                console.log(response);
                                res.send(response.body);
                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })
                        }

                    }
                });


            } else {
                if (files.gst_upload) {
                    helper.uploadPdfFiles(fields.user_id, files.gst_upload[0].path, files.gst_upload[0].originalFilename, (error1, gstUpload) => {
                        if (error1) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);

                        } else {
                            let dataArray = {
                                country_name: fields.country_check ? fields.country_check[0] : '',
                                state_name: fields.state_name ? fields.state_name[0] : '',
                                city_name: fields.city_name ? fields.city_name[0] : '',
                                billing_address: fields.billing_address ? fields.billing_address[0] : '',
                                pan_number: fields.pan_number ? fields.pan_number[0] : '',
                                gst: fields.gst ? fields.gst[0] : '',
                                gst_number: fields.gst_number ? fields.gst_number[0] : '',
                                sac_code: fields.sac_code ? fields.sac_code[0] : '',
                                nature_service: fields.nature_service ? fields.nature_service[0] : '',
                                state_code: fields.state_code ? fields.state_code[0] : '',
                                ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                                npwp: fields.npwp ? fields.npwp[0] : '',
                                tax_id: fields.tax_id ? fields.tax_id[0] : '',
                                business_relation: fields.business_relation ? fields.business_relation[0] : '',
                                user_id: fields.user_id ? fields.user_id[0] : '',
                                gst_image: gstUpload
                            }
                            const options = {
                                uri: baseUrl + `/api/payment_add`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                console.log(response);
                                res.send(response.body);
                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })

                        }
                    });
                } else {
                    let dataArray = {
                        country_name: fields.country_check ? fields.country_check[0] : '',
                        state_name: fields.state_name ? fields.state_name[0] : '',
                        city_name: fields.city_name ? fields.city_name[0] : '',
                        billing_address: fields.billing_address ? fields.billing_address[0] : '',
                        pan_number: fields.pan_number ? fields.pan_number[0] : '',
                        gst: fields.gst ? fields.gst[0] : '',
                        gst_number: fields.gst_number ? fields.gst_number[0] : '',
                        sac_code: fields.sac_code ? fields.sac_code[0] : '',
                        nature_service: fields.nature_service ? fields.nature_service[0] : '',
                        state_code: fields.state_code ? fields.state_code[0] : '',
                        ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                        npwp: fields.npwp ? fields.npwp[0] : '',
                        tax_id: fields.tax_id ? fields.tax_id[0] : '',
                        business_relation: fields.business_relation ? fields.business_relation[0] : '',
                        user_id: fields.user_id ? fields.user_id[0] : '',
                    }
                    const options = {
                        uri: baseUrl + `/api/payment_add`,
                        json: dataArray,
                        resolveWithFullResponse: true,
                        method: 'POST'
                    };
                    return request(options).then((response) => {
                        console.log(response);
                        res.send(response.body);
                    }).catch((err) => {
                        console.log(err);
                        console.log('errorstatuscode:' + err.statusCode)
                    })
                }
            }
        } else if (fields.country_check == "Indonesia") {
            if (files.e_ktp_upload) {
                helper.uploadPdfFiles(fields.user_id, files.e_ktp_upload[0].path, files.e_ktp_upload[0].originalFilename, (error, ktpUpload) => {
                    if (error) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error);
                    } else {
                        if (files.npwp_upload) {
                            helper.uploadPdfFiles(fields.user_id, files.npwp_upload[0].path, files.npwp_upload[0].originalFilename, (error1, npwpUpload) => {
                                if (error1) {
                                    responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);

                                } else {
                                    let dataArray = {
                                        country_name: fields.country_check ? fields.country_check[0] : '',
                                        state_name: fields.state_name ? fields.state_name[0] : '',
                                        city_name: fields.city_name ? fields.city_name[0] : '',
                                        billing_address: fields.billing_address ? fields.billing_address[0] : '',
                                        pan_number: fields.pan_number ? fields.pan_number[0] : '',
                                        gst: fields.gst ? fields.gst[0] : '',
                                        gst_number: fields.gst_number ? fields.gst_number[0] : '',
                                        sac_code: fields.sac_code ? fields.sac_code[0] : '',
                                        nature_service: fields.nature_service ? fields.nature_service[0] : '',
                                        state_code: fields.state_code ? fields.state_code[0] : '',
                                        ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                                        npwp: fields.npwp ? fields.npwp[0] : '',
                                        tax_id: fields.tax_id ? fields.tax_id[0] : '',
                                        business_relation: fields.business_relation ? fields.business_relation[0] : '',
                                        user_id: fields.user_id ? fields.user_id[0] : '',
                                        ktp_image: ktpUpload,
                                        npwp_image: npwpUpload
                                    }
                                    const options = {
                                        uri: baseUrl + `/api/payment_add`,
                                        json: dataArray,
                                        resolveWithFullResponse: true,
                                        method: 'POST'
                                    };
                                    return request(options).then((response) => {
                                        console.log(response);
                                        res.send(response.body);
                                    }).catch((err) => {
                                        console.log(err);
                                        console.log('errorstatuscode:' + err.statusCode)
                                    })

                                }
                            });
                        } else {
                            let dataArray = {
                                country_name: fields.country_check ? fields.country_check[0] : '',
                                state_name: fields.state_name ? fields.state_name[0] : '',
                                city_name: fields.city_name ? fields.city_name[0] : '',
                                billing_address: fields.billing_address ? fields.billing_address[0] : '',
                                pan_number: fields.pan_number ? fields.pan_number[0] : '',
                                gst: fields.gst ? fields.gst[0] : '',
                                gst_number: fields.gst_number ? fields.gst_number[0] : '',
                                sac_code: fields.sac_code ? fields.sac_code[0] : '',
                                nature_service: fields.nature_service ? fields.nature_service[0] : '',
                                state_code: fields.state_code ? fields.state_code[0] : '',
                                ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                                npwp: fields.npwp ? fields.npwp[0] : '',
                                tax_id: fields.tax_id ? fields.tax_id[0] : '',
                                business_relation: fields.business_relation ? fields.business_relation[0] : '',
                                user_id: fields.user_id ? fields.user_id[0] : '',
                                ktp_image: ktpUpload
                            }
                            const options = {
                                uri: baseUrl + `/api/payment_add`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                console.log(response);
                                res.send(response.body);
                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })
                        }

                    }
                });


            } else {
                if (files.npwp_upload) {
                    helper.uploadPdfFiles(fields.user_id, files.npwp_upload[0].path, files.npwp_upload[0].originalFilename, (error1, npwpUpload) => {
                        if (error1) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);

                        } else {
                            let dataArray = {
                                country_name: fields.country_check ? fields.country_check[0] : '',
                                state_name: fields.state_name ? fields.state_name[0] : '',
                                city_name: fields.city_name ? fields.city_name[0] : '',
                                billing_address: fields.billing_address ? fields.billing_address[0] : '',
                                pan_number: fields.pan_number ? fields.pan_number[0] : '',
                                gst: fields.gst ? fields.gst[0] : '',
                                gst_number: fields.gst_number ? fields.gst_number[0] : '',
                                sac_code: fields.sac_code ? fields.sac_code[0] : '',
                                nature_service: fields.nature_service ? fields.nature_service[0] : '',
                                state_code: fields.state_code ? fields.state_code[0] : '',
                                ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                                npwp: fields.npwp ? fields.npwp[0] : '',
                                tax_id: fields.tax_id ? fields.tax_id[0] : '',
                                business_relation: fields.business_relation ? fields.business_relation[0] : '',
                                user_id: fields.user_id ? fields.user_id[0] : '',
                                npwp_image: npwpUpload
                            }
                            const options = {
                                uri: baseUrl + `/api/payment_add`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                console.log(response);
                                res.send(response.body);
                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })

                        }
                    });
                } else {
                    let dataArray = {
                        country_name: fields.country_check ? fields.country_check[0] : '',
                        state_name: fields.state_name ? fields.state_name[0] : '',
                        city_name: fields.city_name ? fields.city_name[0] : '',
                        billing_address: fields.billing_address ? fields.billing_address[0] : '',
                        pan_number: fields.pan_number ? fields.pan_number[0] : '',
                        gst: fields.gst ? fields.gst[0] : '',
                        gst_number: fields.gst_number ? fields.gst_number[0] : '',
                        sac_code: fields.sac_code ? fields.sac_code[0] : '',
                        nature_service: fields.nature_service ? fields.nature_service[0] : '',
                        state_code: fields.state_code ? fields.state_code[0] : '',
                        ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                        npwp: fields.npwp ? fields.npwp[0] : '',
                        tax_id: fields.tax_id ? fields.tax_id[0] : '',
                        business_relation: fields.business_relation ? fields.business_relation[0] : '',
                        user_id: fields.user_id ? fields.user_id[0] : '',
                    }
                    const options = {
                        uri: baseUrl + `/api/payment_add`,
                        json: dataArray,
                        resolveWithFullResponse: true,
                        method: 'POST'
                    };
                    return request(options).then((response) => {
                        console.log(response);
                        res.send(response.body);
                    }).catch((err) => {
                        console.log(err);
                        console.log('errorstatuscode:' + err.statusCode)
                    })
                }
            }
        } else {

            if (files.tax_upload) {
                helper.uploadPdfFiles(fields.user_id, files.tax_upload[0].path, files.tax_upload[0].originalFilename, (error1, taxpUpload) => {
                    if (error1) {
                        responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error1);
                    } else {
                        let dataArray = {
                            country_name: fields.country_check ? fields.country_check[0] : '',
                            state_name: fields.state_name ? fields.state_name[0] : '',
                            city_name: fields.city_name ? fields.city_name[0] : '',
                            billing_address: fields.billing_address ? fields.billing_address[0] : '',
                            pan_number: fields.pan_number ? fields.pan_number[0] : '',
                            gst: fields.gst ? fields.gst[0] : '',
                            gst_number: fields.gst_number ? fields.gst_number[0] : '',
                            sac_code: fields.sac_code ? fields.sac_code[0] : '',
                            nature_service: fields.nature_service ? fields.nature_service[0] : '',
                            state_code: fields.state_code ? fields.state_code[0] : '',
                            ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                            npwp: fields.npwp ? fields.npwp[0] : '',
                            tax_id: fields.tax_id ? fields.tax_id[0] : '',
                            business_relation: fields.business_relation ? fields.business_relation[0] : '',
                            user_id: fields.user_id ? fields.user_id[0] : '',
                            tax_image: taxpUpload
                        }
                        const options = {
                            uri: baseUrl + `/api/payment_add`,
                            json: dataArray,
                            resolveWithFullResponse: true,
                            method: 'POST'
                        };
                        return request(options).then((response) => {
                            console.log(response);
                            res.send(response.body);
                        }).catch((err) => {
                            console.log(err);
                            console.log('errorstatuscode:' + err.statusCode)
                        })
                    }
                });
            } else {
                let dataArray = {
                    country_name: fields.country_check ? fields.country_check[0] : '',
                    state_name: fields.state_name ? fields.state_name[0] : '',
                    city_name: fields.city_name ? fields.city_name[0] : '',
                    billing_address: fields.billing_address ? fields.billing_address[0] : '',
                    pan_number: fields.pan_number ? fields.pan_number[0] : '',
                    gst: fields.gst ? fields.gst[0] : '',
                    gst_number: fields.gst_number ? fields.gst_number[0] : '',
                    sac_code: fields.sac_code ? fields.sac_code[0] : '',
                    nature_service: fields.nature_service ? fields.nature_service[0] : '',
                    state_code: fields.state_code ? fields.state_code[0] : '',
                    ktp_number: fields.ktp_number ? fields.ktp_number[0] : '',
                    npwp: fields.npwp ? fields.npwp[0] : '',
                    tax_id: fields.tax_id ? fields.tax_id[0] : '',
                    business_relation: fields.business_relation ? fields.business_relation[0] : '',
                    user_id: fields.user_id ? fields.user_id[0] : '',
                }
                const options = {
                    uri: baseUrl + `/api/payment_add`,
                    json: dataArray,
                    resolveWithFullResponse: true,
                    method: 'POST'
                };
                return request(options).then((response) => {
                    console.log(response);
                    res.send(response.body);
                }).catch((err) => {
                    console.log(err);
                    console.log('errorstatuscode:' + err.statusCode)
                })
            }
        }
    });

}

/**
 * [Phone check]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const phoneCheck = (req, res) => {
    const options = {
        uri: baseUrl + `/api/phone/check`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Adding Social Connection]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */


const addconnection = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/addconnection`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    }
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Send otp]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const sendOTP = (req, res) => {
    const options = {
        uri: baseUrl + `/api/send/OTP`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [verify otp]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const verifyOTP = (req, res) => {
    const options = {
        uri: baseUrl + `/api/verify/OTP`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Add user bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addUserBankData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user/bank`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Delete Social Connection]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const deleteConnection = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/deleteconnection`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Delete Social Connection]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const deleteSocials = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/delete/socials`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [get Country List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const getCountryList = (req, res) => {
    const options = {
        uri: baseUrl + `/api/countrylist`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [state list based on country]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getStateList = (req, res) => {
    const options = {
        uri: baseUrl + `/api/statelist`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

const getCurrencyList = (req, res) => {
    const options = {
        uri: baseUrl + `/api/currency/list`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [get Country List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const getCountryISD = (req, res) => {
    const options = {
        uri: baseUrl + `/api/country/isd`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [ Notifications ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const notificationSettings = (req, res) => {
    const options = {
        uri: baseUrl + `/api/notification`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'PUT'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [ Get City ]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getCitiesList = (req, res) => {
    const options = {
        uri: baseUrl + `/api/citylist`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}
    /**
     * [User Twitter auth]
     * @param {[type]} req [object received from the application.]
     * @param {[type]} res [object to be sent as response from the api.]
     * @return {[type]} [function call to return the appropriate response]
     */
const twitterAuth = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        method: 'GET',
        uri: baseUrl + `/api/twitterconnection`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Twitter callback]
 * @param {[type]} req [object received from the application.]
 * @param {[type]} res [object to be sent as response from the api.]
 * @return {[type]} [function call to return the appropriate response]
 */
const twitterCallback = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/twitterdata`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [linked in login handle user to be login from the social]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const linkedInLogin = (req, res) => {
    const options = {
        uri: baseUrl + `/api/linkedin/`,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [linked in callback handle callback from linked in login from the api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const linkedInCallBack = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/linkedin-callback/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [youtube login handle user to be login from the social page]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const youtubeLogin = (req, res) => {
    const options = {
        uri: baseUrl + `/api/youtube/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [youtube callback handle callback url from youtube login api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const youtubeCallBack = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/youtube-callback/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [Add Instagram data from  login api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const instagramAddData = (req, res) => {
    // console.log(req.body);
    // return false;
    const options = {
        uri: baseUrl + `/api/instagramlogin/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Add Instagram data from  login api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const linkedinAddData = (req, res) => {
    // console.log(req.body);
    // return false;
    const options = {
        uri: baseUrl + `/api/linkedinlogin/`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Add Instagram data from  login api]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const youtubeAddData = (req, res) => {
        // console.log(req.body);
        // return false;
        const options = {
            uri: baseUrl + `/api/youtubelogin/`,
            json: req.body,
            resolveWithFullResponse: true,
            method: 'POST'
        };
        return request(options).then((response) => {
            if (response.body) {
                res.send(response.body);
            } else {
                res.send({ 'responseCode': 404, 'responseMessage': 'error' });
            }
        }).catch((err) => {
            res.send({ 'responseCode': 500, 'responseMessage': 'error' });
        })
    }
/**
 * [User Blog Add]
 * @param {[type]} req [object received from the application.]
 * @param {[type]} res [object to be sent as response from the api.]
 * @return {[type]} [function call to return the appropriate response]
 */
const addBlog = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/user/blog`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}
    /**
     * [User Delete Add]
     * @param {[type]} req [object received from the application.]
     * @param {[type]} res [object to be sent as response from the api.]
     * @return {[type]} [function call to return the appropriate response]
     */
const deleteBlog = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user/blog/delete`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [Set default user bank data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const defaultUserBankData = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user/bank/default`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'PUT'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userImgaeUpload = (req, res) => {
    const options = {
        uri: baseUrl + `/api/user/image/upload`,
        json: req.body,
        resolveWithFullResponse: true,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [User fb auth]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const instagramBusinessLogin = (req, res) => {
    const options = {
        method: 'GET',
        uri: baseUrl + `/api/instagram/business`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [FB callback]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const instagramBusinessCallBack = (req, res) => {
    console.log(req);
    const options = {
        method: 'POST',
        uri: baseUrl + `/api/instagram-business/callback`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [instagram SignIn on Login Page]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const instagramSignin = (req, res) => {

    const options = {
        uri: baseUrl + `/api/logininstagram/`,
        json: true,
        resolveWithFullResponse: true,
        method: 'GET'
    };
    return request(options).then((response) => {

        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [instagramCallBack  on signin Page]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const instagramSigninCallBack = (req, res) => {

    const options = {
        uri: baseUrl + `/api/logininstagram-callback/`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Advertiser Create  on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const advertiserCreate = (req, res) => {

    const options = {
        uri: baseUrl + `/api/advertiser/create`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Advertiser Image Upload  on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const advertiserProfileUpload = (req, res) => {

    const options = {
        uri: baseUrl + `/api/advertiser/imageupload`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [AdvertiserEdit  on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const advertiserEdit = (req, res) => {

    const options = {
        uri: baseUrl + `/api/advertiser/edit`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [status Update of Advertiser  on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const statusUpdate = (req, res) => {

    const options = {
        uri: baseUrl + `/api/advertiser/status`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Organisation Details on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const organisationDetails = (req, res) => {

    const options = {
        uri: baseUrl + `/api/organisation/details`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

// excelToJson
const excelToJsonfn = async(req, res) => {
    const multiparty = require('multiparty');
    const xlxsData = new multiparty.Form();
    xlxsData.parse(req, function(err, fields, files) {
        var file_path = files.file[0].path;
        csv()
            .fromFile(file_path)
            .then((jsonObj) => {
                var dataArray = {
                    "data": jsonObj
                }
                const options = {
                    // headers: { 'campaign_id': campaignId },
                    uri: baseUrl + `/api/influencer/excelupload`,
                    json: dataArray,
                    resolveWithFullResponse: true,
                    method: 'POST'
                };
                return request(options).then((response) => {
                    if (response.body) {
                        res.send(response.body);
                    } else {
                        res.send({ 'responseCode': 404, 'responseMessage': 'error' });
                    }
                }).catch((err) => {
                    res.send({ 'responseCode': 500, 'responseMessage': 'error' });
                })
            });
    });

}

/**
 * [Organisation Details on Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const defaultSkillSetting = (req, res) => {
    req.body.decoded = req.decoded;
    const options = {
        uri: baseUrl + `/api/skilldefaultSetting`,
        json: true,
        resolveWithFullResponse: true,
        json: req.body,
        method: 'POST'
    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/*Export apis*/
module.exports = {
    register,
    subscribe,
    login,
    logout,
    facebookAuth,
    facebookCallback,
    twitterCallback,
    forgotPassFunction,
    resetFunction,
    instahandleauth,
    checkExpiryFunction,
    changepassword,
    currentpassword,
    addUserSkills,
    getUserData,
    getCountryList,
    getStateList,
    getCitiesList,
    updateUserData,
    checkemailverification,
    emailverify,
    email_send_link,
    addconnection,
    getUserBankData,
    addUserBankData,
    editUserBankData,
    deleteUserBankData,
    getUserPaymentData,
    addUserPaymentData,
    phoneCheck,
    sendOTP,
    verifyOTP,
    linkedInCallBack,
    linkedInLogin,
    youtubeLogin,
    youtubeCallBack,
    notificationSettings,
    deleteConnection,
    deleteSocials,
    twitterAuth,
    instagramLogin,
    instagramCallBack,
    instagramAddData,
    linkedinAddData,
    youtubeAddData,
    getCountryISD,
    deleteBlog,
    defaultUserBankData,
    userImgaeUpload,
    addBlog,
    instagramBusinessLogin,
    instagramBusinessCallBack,
    instagramSignin,
    instagramSigninCallBack,
    advertiserCreate,
    advertiserProfileUpload,
    advertiserEdit,
    statusUpdate,
    organisationDetails,
    getCurrencyList,
    excelToJsonfn,
    defaultSkillSetting
}