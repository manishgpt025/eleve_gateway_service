const bloggerController = require("../controllers/bloggerController");
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();

// List
router.post('/list', checkToken, bloggerController.getBloggerList);

/*Export*/
module.exports = router;