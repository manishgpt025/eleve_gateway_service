const userController = require("../controllers/userController");
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();
// Sign up
router.post('/register', userController.register);
// USER web push notification Subscribe
router.post('/subscribe', checkToken, userController.subscribe);
// Login
router.post('/login', userController.login);
router.post('/logout', userController.logout);

// Twitter
router.get('/twitter/auth',checkToken, userController.twitterAuth);
router.post('/twitter/callback',checkToken, userController.twitterCallback);

// Facebook connect
router.get('/facebook/auth',checkToken, userController.facebookAuth);
router.post('/facebook/callback',checkToken, userController.facebookCallback);

// linked in login
router.get('/linkedin/auth',checkToken, userController.linkedInLogin);
router.post('/linkedin/callback',checkToken, userController.linkedInCallBack);
// youtube in login
router.get('/youtube/auth',checkToken, userController.youtubeLogin);
router.post('/youtube/callback',checkToken, userController.youtubeCallBack);
// instagram in login
router.post('/instagram/auth', userController.instagramLogin);
router.post('/instagram/callback', userController.instagramCallBack);
// instagram business in login
router.post('/instagram/business/auth', userController.instagramBusinessLogin);
router.post('/instagram/business/callback', userController.instagramBusinessCallBack);

// Reset password
router.post('/reset-password', userController.resetFunction);
// Forget password
router.post('/forgot-password', userController.forgotPassFunction);
// Insta login
router.post('/insta/handleauth', userController.instahandleauth);
// router.get('/instagram/login',userController.instagramlogin);
router.post('/check-url-expiry', userController.checkExpiryFunction);
// Change password
router.post('/changepassword', userController.changepassword);
router.post('/checkpassword', userController.currentpassword);
// notification
router.post('/notification', userController.notificationSettings);
// User Skills /user
router.post('/user', checkToken, userController.getUserData);
router.post('/user/update', checkToken, userController.updateUserData);
router.post('/user/skills', checkToken, userController.addUserSkills);
router.post('/user/image/upload', checkToken, userController.userImgaeUpload);
//country
router.get('/country', userController.getCountryList);
router.get('/state', userController.getStateList);
router.get('/currency/list', userController.getCurrencyList);
//email verify
router.post('/checkemailverification', userController.checkemailverification);
router.post('/email_verify', userController.emailverify);
router.post('/email_send_link', userController.email_send_link);
//socialconnection
router.post('/addconnection',checkToken, userController.addconnection);
router.post('/deleteconnection', checkToken,userController.deleteConnection);
router.post('/delete/socials', checkToken, userController.deleteSocials);
// Phone verification
router.post('/phone/check', checkToken, userController.phoneCheck);
router.post('/send/OTP', checkToken, userController.sendOTP);
router.post('/verify/OTP', checkToken, userController.verifyOTP);
// User Payment and Bank Details
router.post('/user/payment-data', checkToken, userController.getUserPaymentData);
router.post('/user/add-payment-data', checkToken, userController.addUserPaymentData);
// Bank details
router.post('/user/bank', checkToken, userController.getUserBankData);
router.post('/user/add-bank-details', checkToken, userController.addUserBankData);
router.post('/user/edit-bank-details', checkToken, userController.editUserBankData);
router.post('/user/delete-bank-details', checkToken, userController.deleteUserBankData);
router.post('/user/default-bank-details', checkToken, userController.defaultUserBankData);
// Country , State & cities List api
router.get('/countries', userController.getCountryList);
router.post('/statelist', userController.getStateList);
router.post('/cities', userController.getCitiesList);
router.get('/country/isd', userController.getCountryISD);

//add blog
router.post('/user/blog',checkToken, userController.addBlog);
//delete blog
router.post('/user/blog/delete', userController.deleteBlog);

//Sign in with Instagram
router.get('/instagram/signin', userController.instagramSignin);
router.post('/instagram/signincallback', userController.instagramSigninCallBack);

//advertiser Api
router.post('/advertisercreate', checkToken, userController.advertiserCreate);
router.post('/advertiserprofileimage', userController.advertiserProfileUpload);
router.post('/advertiseredit', checkToken, userController.advertiserEdit);
router.post('/advertiser/status', checkToken, userController.statusUpdate);
// organisation details
router.post('/organisation/details', checkToken, userController.organisationDetails);
//
router.post('/upload/xlxs', checkToken, userController.excelToJsonfn);
//Default Skill Setting
router.get('/all_skill_setting', checkToken, userController.defaultSkillSetting);

/*Export*/
module.exports = router;