const contentController = require("../controllers/contentController");
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();
// upload videos id from csv
router.post('/youtube_upload_video',checkToken, contentController.youtubeUploadVideo);
// upload channels id from csv
router.post('/youtube_upload_channel',checkToken, contentController.uploadYoutubeChannel);
// Get all Channels data
router.get('/get_channel_data',checkToken, contentController.getChannelsData);
/*Export*/
module.exports = router;