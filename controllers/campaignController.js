const request = require('request-promise');
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const userbaseUrl = global.gConfig.base_url;
const campaignbaseUrl = global.gConfig.campaign_base_url;
//use helper
const helper = require('../globalFunctions/function.js');
const secret = `${global.gConfig.secret}`;
// CSV to JSON
const csv = require('csvtojson');
// JWT
let jwt = require('jsonwebtoken');
var array = [];
/**
 * [List to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const advertiserlist = (req, res) => {

    var adverListPromise = new Promise(function(resolve, reject) {

        const options = {
            // headers: { 'user_id': req.headers['user_id'] },
            method: 'POST',
            uri: userbaseUrl + `/api/advertisers`,
            json: req.body,
            resolveWithFullResponse: true,

        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adverListPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [List to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const advertiserDetails = (req, res) => {

    var adverDetailPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/advertiser/details`,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            resolveWithFullResponse: true,

        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adverDetailPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [organisation to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const addOrganisation = (req, res) => {

    var addOrganisationPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/add-organisation`,
            json: req.body,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] }

        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    addOrganisationPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [search organization from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchOrganisation = (req, res) => {

    var searchPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/searchOrganisation`,
            json: req.body,
            resolveWithFullResponse: true,

        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}


/**
 * [Organisation  List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const organisationlist = (req, res) => {
    var listPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/organisations`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    listPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [update organisation to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateOrganisation = (req, res) => {

    var updateOrganisationPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/update-organisation`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    updateOrganisationPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [update organisation to be created from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const updateLocationOrganisation = (req, res) => {

    var updateLocationPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/location-organisation`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    updateLocationPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [organisation to be get from the module]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getOrganisation = (req, res) => {

    var getOrganisationPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/get-organisation`,
            json: req.body,
            resolveWithFullResponse: true
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getOrganisationPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
    }
    /**
     * [get all admin users list Admin Section]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */

const getAdminList = (req, res) => {

    var getAdminPromise = new Promise(function(resolve, reject) {
        req.body.decoded = req.decoded;
        const options = {
            uri: userbaseUrl + `/api/admin/list`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getAdminPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Admin Login Admin Section]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminLogin = (req, res) => {
    let ip = req.headers['cf-connecting-ip']
                ||
            req.headers['x-forwarded-for']
                ||
            req.connection.remoteAddress;
    req.body.login_ip = ip;
    var adminLoginPromise = new Promise(function(resolve, reject) {
        const options = {
            uri: userbaseUrl + `/api/admin/login`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                // JWT
                let token = jwt.sign({ email: req.body.email, type: 1 },
                secret, {
                    expiresIn: '24h' // expires in 24 hours
                });
                response.body.token = token;
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminLoginPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Admin create]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminCreate = (req, res) => {

    var adminCreatePromise = new Promise(function(resolve, reject) {

        const options = {
            uri: userbaseUrl + `/api/createadmin`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminCreatePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Admin status update]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminStatusUpdate = (req, res) => {
    var adminStatusPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: userbaseUrl + `/api/admin/statusupdate`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminStatusPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Admin  update]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminUpdate = (req, res) => {
    var adminUpdatePromise = new Promise(function(resolve, reject) {
        const options = {
            uri: userbaseUrl + `/api/admin/update`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminUpdatePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
    }
    /**
     * [Admin  update]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */

const adminPasswordUpdate = (req, res) => {

    var adminPasswordPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: userbaseUrl + `/api/admin/passwordupdate`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminPasswordPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Admin User data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminUserData = (req, res) => {
    var adminUserPromise = new Promise(function(resolve, reject) {
        req.body.decoded = req.decoded;
        const options = {
            uri: userbaseUrl + `/api/admin/getdata`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminUserPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Admin User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminImageUpload = (req, res) => {
    var adminImagePromise = new Promise(function(resolve, reject) {
        const options = {
            uri: userbaseUrl + `/api/admin/imageupload`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })
    adminImagePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}


/**
 * [Admin User Image Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const adminChangePassword = (req, res) => {
    var changePasswordPromise = new Promise(function(resolve, reject) {
        req.body.decoded = req.decoded;
        const options = {
            uri: userbaseUrl + `/api/admin/changepassword`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    changePasswordPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

const adminUserList = (req, res) => {
    var adminListPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: userbaseUrl + `/api/users/list`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            // json: req.body,
            method: 'GET'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    adminListPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}


/**
 * [Campaign Discard]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const campaignDiscard = (req, res) => {

    var campDiscardPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/discard`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campDiscardPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign Completed]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const campaignCompleted = (req, res) => {
    var campCompletedPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/completed`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campCompletedPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign delete]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const campaignDelete = (req, res) => {
    var campDeletePromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/delete`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campDeletePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign platform]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignPlatformDel = (req, res) => {
    var campPlatformPromise = new Promise(function(resolve, reject) {
        req.body.decoded = req.decoded;
        const options = {
            uri: campaignbaseUrl + `/campaign/platform/delete`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campPlatformPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign status update]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignEditStatus = (req, res) => {
    var campEditPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/edit/status`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campEditPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign Influencers]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignInfluencers = (req, res) => {
    var campInfluencerPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/influencers`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campInfluencerPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}


/**
 * [Campaign Manager List]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const camapignManagerList = (req, res) => {

    var campManagerPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/managerlist`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campManagerPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [create Campaign]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */

const create_campaign = async(req, res) => {
    if (req.headers.steptype) {
        var cases = req.headers.steptype;
        switch (cases) {
            case "1":
                const multiparty = require('multiparty');
                const campaignForm = new multiparty.Form();
                campaignForm.parse(req, function(err, fields, files) {
                    if (fields.campaign_title) {
                        if (fields.campaign_id && fields.campaign_id !== undefined) {
                            var campaignId = fields.campaign_id;
                        } else {
                            var campaignId = "";
                        }
                        if (fields.secondary_hashtag && fields.secondary_hashtag[0] != '') {
                            var secondryHashtag = [];
                            fields.secondary_hashtag.forEach(function(sHashtag) {
                                secondryHashtag.push(sHashtag);
                            });
                        }
                        if (fields.keywords && fields.keywords[0] != '') {
                            var keywords = [];
                            fields.keywords.forEach(function(keywordsData) {
                                keywords.push(keywordsData);
                            });
                        }
                        if (fields.retweet_url && fields.retweet_url[0] != '') {
                            var retweetUrl = [];
                            var retweetIds = [];
                            fields.retweet_url.forEach(function(retweetUrlData) {
                                retweetUrl.push(retweetUrlData);
                                let retweetId = retweetUrlData.split("/");
                                retweetIds.push(retweetId[3]);
                            });
                        }
                        if (fields.colleagues[0] != '' && fields.colleagues) {
                            var colleaguesData = [];
                            fields.colleagues.forEach(function(colleaguesValue) {
                                colleaguesData.push(colleaguesValue);
                            });
                        }
                        if (fields.image && (fields.image.length > 0 && fields.image.length <= 50) && fields.image[0] != '') {
                            var arrayImage = [];
                            fields.image.forEach((item) => {
                                arrayImage.push({ "url": item });
                            });
                            let dataArray = {
                                title: fields.campaign_title[0],
                                type: fields.objective_type != "" ? fields.objective_type[0] : "",
                                hashtag: fields.primary_hashtag[0] != '' ? fields.primary_hashtag[0] : "",
                                hashtag_share: fields.primary_hashtag_share[0] == 'true' ? true : false,
                                url: fields.url != "" ? fields.url[0] : "",
                                url_share: fields.url_share[0] == 'true' ? true : false,
                                secondary_hashtag: fields.secondary_hashtag[0] != '' ? secondryHashtag : [],
                                track_secondary_hashtag: fields.secondary_hashtag_track[0] == 'true' ? true : false,
                                keywords: fields.keywords[0] != '' ? keywords : [],
                                keywords_share: fields.keywords_share[0] == 'true' ? true : false,
                                track_keywords: fields.keywords_track[0] == 'true' ? true : false,
                                retweet_url: fields.retweet_url[0] != '' ? retweetUrl : [],
                                retweet_id: fields.retweet_url[0] != '' ? retweetIds : [],
                                images: arrayImage,
                                video: "",
                                media_share: fields.media_share[0] == 'true' ? true : false,
                                advertiser_type: fields.advertiser_type != "" ? fields.advertiser_type[0] : "",
                                organization_id: fields.agency_id != "" ? fields.agency_id[0] : "",
                                advertiser_id: fields.advertiser_id != "" ? fields.advertiser_id[0] : "",
                                advertiser_brand_id: fields.brand_id != "" ? fields.brand_id[0] : "",
                                organization_name: fields.agency_id != "" ? fields.agency_name[0] : "",
                                advertiser_name: fields.advertiser_id != "" ? fields.advertiser_name[0] : "",
                                advertiser_brand_name: fields.brand_id != "" ? fields.brand_name[0] : "",
                                ro_number: fields.ro_number != "" ? fields.ro_number[0] : "",
                                eo_number: fields.eo_number != "" ? fields.eo_number[0] : "",
                                manager_id: fields.manager_id != "" ? fields.manager_id[0] : "",
                                colleagues: fields.colleagues[0] != '' ? colleaguesData : [],
                                status: fields.status == '1' ? "0" : fields.status[0]
                            }
                            const options = {
                                headers: { 'campaign_id': campaignId },
                                uri: campaignbaseUrl + `/campaign/create_campaign_step_one`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                res.send(response.body);
                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })
                        } else if (fields.video) {
                            let dataArray = {
                                title: fields.campaign_title[0],
                                type: fields.objective_type != "" ? fields.objective_type[0] : "",
                                hashtag: fields.primary_hashtag[0] != '' ? fields.primary_hashtag[0] : "",
                                hashtag_share: fields.primary_hashtag_share[0] == 'true' ? true : false,
                                url: fields.url != "" ? fields.url[0] : "",
                                url_share: fields.url_share[0] == 'true' ? true : false,
                                secondary_hashtag: fields.secondary_hashtag[0] != '' ? secondryHashtag : [],
                                track_secondary_hashtag: fields.secondary_hashtag_track[0] == 'true' ? true : false,
                                keywords: fields.keywords[0] != '' ? keywords : [],
                                keywords_share: fields.keywords_share[0] == 'true' ? true : false,
                                track_keywords: fields.keywords_track[0] == 'true' ? true : false,
                                retweet_url: fields.retweet_url[0] != "" ? retweetUrl : [],
                                retweet_id: fields.retweet_url[0] != "" ? retweetIds : [],
                                images: [],
                                video: fields.video ? fields.video[0] : "",
                                media_share: fields.media_share[0] == 'true' ? true : false,
                                advertiser_type: fields.advertiser_type != "" ? fields.advertiser_type[0] : "",
                                organization_id: fields.agency_id != "" ? fields.agency_id[0] : "",
                                advertiser_id: fields.advertiser_id != "" ? fields.advertiser_id[0] : "",
                                advertiser_brand_id: fields.brand_id != "" ? fields.brand_id[0] : "",
                                organization_name: fields.agency_id != "" ? fields.agency_name[0] : "",
                                advertiser_name: fields.advertiser_id != "" ? fields.advertiser_name[0] : "",
                                advertiser_brand_name: fields.brand_id != "" ? fields.brand_name[0] : "",
                                ro_number: fields.ro_number != "" ? fields.ro_number[0] : "",
                                eo_number: fields.eo_number != "" ? fields.eo_number[0] : "",
                                manager_id: fields.manager_id != "" ? fields.manager_id[0] : "",
                                colleagues: fields.colleagues[0] != '' ? colleaguesData : [],
                                status: fields.status == '1' ? "0" : fields.status[0]
                            }
                            const options = {
                                headers: { 'campaign_id': campaignId },
                                uri: campaignbaseUrl + `/campaign/create_campaign_step_one`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                    res.send(response.body);
                                }).catch((err) => {
                                    console.log('errorstatuscode:' + err.statusCode)
                                })
                        } else {
                            let dataArray = {
                                title: fields.campaign_title[0],
                                type: fields.objective_type != "" ? fields.objective_type[0] : "",
                                hashtag: fields.primary_hashtag[0] != '' ? fields.primary_hashtag[0] : "",
                                hashtag_share: fields.primary_hashtag_share[0] == 'true' ? true : false,
                                url: fields.url != "" ? fields.url[0] : "",
                                url_share: fields.url_share[0] == 'true' ? true : false,
                                secondary_hashtag: fields.secondary_hashtag[0] != '' ? secondryHashtag : [],
                                track_secondary_hashtag: fields.secondary_hashtag_track[0] == 'true' ? true : false,
                                keywords: fields.keywords[0] != '' ? keywords : [],
                                keywords_share: fields.keywords_share[0] == 'true' ? true : false,
                                track_keywords: fields.keywords_track[0] == 'true' ? true : false,
                                retweet_url: fields.retweet_url[0] != '' ? retweetUrl : [],
                                retweet_id: fields.retweet_url[0] != '' ? retweetIds : [],
                                media_share: fields.media_share[0] == 'true' ? true : false,
                                images: [],
                                video: "",
                                advertiser_type: fields.advertiser_type != "" ? fields.advertiser_type[0] : "",
                                organization_id: fields.agency_id != "" ? fields.agency_id[0] : "",
                                advertiser_id: fields.advertiser_id != "" ? fields.advertiser_id[0] : "",
                                advertiser_brand_id: fields.brand_id != "" ? fields.brand_id[0] : "",
                                organization_name: fields.agency_id != "" ? fields.agency_name[0] : "",
                                advertiser_name: fields.advertiser_id != "" ? fields.advertiser_name[0] : "",
                                advertiser_brand_name: fields.brand_id != "" ? fields.brand_name[0] : "",
                                ro_number: fields.ro_number != "" ? fields.ro_number[0] : "",
                                eo_number: fields.eo_number != "" ? fields.eo_number[0] : "",
                                manager_id: fields.manager_id != "" ? fields.manager_id[0] : "",
                                colleagues: fields.colleagues[0] != '' ? colleaguesData : [],
                                status: fields.status == '1' ? "0" : fields.status[0]
                            }
                            const options = {
                                headers: { 'campaign_id': campaignId },
                                uri: campaignbaseUrl + `/campaign/create_campaign_step_one`,
                                json: dataArray,
                                resolveWithFullResponse: true,
                                method: 'POST'
                            };
                            return request(options).then((response) => {
                                res.send(response.body);
                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })
                        }
                    } else {
                        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide campaign name.');
                    }
                });
                break;
            case "2B":
                if (req.body.campaign_id && req.body.campaign_id !== undefined) {

                    let dataArray = {
                        campaign_id: req.body.campaign_id,
                        platform: req.body.platform,
                        platform_id: req.body.platform_id,
                        type: req.body.type,
                        maxInfluencer: req.body.max_influencer,
                        postPerKOL: req.body.post_per_kol,
                        conversations: req.body.conversations,
                        influencers: req.body.influencers,
                        mentions: req.body.mentions,
                        genderspecific: req.body.genderSpecific == "1" ? "1" : "0",
                        commonDescription: req.body.common_description,
                        maleDescription: req.body.male_description,
                        femaleDescription: req.body.female_description,
                        otherDescription: req.body.other_description,
                        commonSample: req.body.common_sample,
                        maleSample: req.body.male_sample,
                        femaleSample: req.body.female_sample,
                        otherSample: req.body.other_sample,
                        copyBriefDescription: req.body.copy_brief_description ? req.body.copy_brief_description : "",
                        status: req.body.status == '1' ? "0" : req.body.status
                    }
                    const options = {
                        uri: campaignbaseUrl + `/campaign/create_campaign_step_two_b`,
                        json: dataArray,
                        resolveWithFullResponse: true,
                        method: 'POST'
                    };
                    return request(options).then((response) => {
                        res.send(response.body);
                    }).catch((err) => {
                        console.log(err);
                        console.log('errorstatuscode:' + err.statusCode)
                    })
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide campaign.');
                }
                break;
            case "2C":
                if (req.body.campaign_id && req.body.campaign_id !== undefined) {
                    let dataArray = {
                        campaign_id: req.body.campaign_id,
                        platform: req.body.platform,
                        platform_id: req.body.platform_id,
                        type: req.body.type,
                        influencerCount: req.body.influencer_count,
                        postCount: req.body.post_count,
                        conversationCount: req.body.post_count,
                        influencers: req.body.influencers,
                        breifToAll :req.body.breif_to_all == '1' ? '1' : '0',
                        status: req.body.status == '1' ? "0" : req.body.status
                    }
                    const options = {
                        uri: campaignbaseUrl + `/campaign/create_campaign_step_two_c`,
                        json: dataArray,
                        resolveWithFullResponse: true,
                        method: 'POST'
                    };
                    return request(options).then((response) => {
                        res.send(response.body);
                    }).catch((err) => {
                        console.log(err);
                        console.log('errorstatuscode:' + err.statusCode)
                    })
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide campaign.');
                }
                break;
            case "3":
                if (req.body.campaign_id && req.body.campaign_id !== undefined) {
                    req.body.decoded = req.decoded;
                    if (req.body.status && req.body.status == "3") {
                        var dataArray = {
                            campaign_id: req.body.campaign_id,
                            endDate: req.body.end_date,
                            created_detail:req.body.decoded
                        }
                    } else {
                        var dataArray = {
                            campaign_id: req.body.campaign_id,
                            startDate: req.body.start_date,
                            endDate: req.body.end_date,
                            invitationStatus: req.body.invitation_status == 1 ? true : false,
                            sendMicro: req.body.send_micro,
                            sendPremium: req.body.send_premium,
                            sendEmail: req.body.send_email,
                            sendSms: req.body.send_sms,
                            emailSender: req.body.email_sender,
                            emailSubject: req.body.email_subject,
                            replyTo: req.body.reply_to,
                            emailBody: req.body.email_body,
                            smsBody: req.body.sms_body,
                            status: req.body.is_draft == '1' ? "0" : req.body.is_draft,
                            created_detail:req.body.decoded
                        }
                    }
                    const options = {
                        uri: campaignbaseUrl + `/campaign/create_campaign_step_three`,
                        json: dataArray,
                        resolveWithFullResponse: true,
                        method: 'POST'
                    };
                    return request(options).then((response) => {
                        res.send(response.body);
                    }).catch((err) => {
                        console.log('errorstatuscode:' + err.statusCode)
                    })
                } else {
                    responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide campaign.');
                }
                break;
            default:
                responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide correct service.');
                break;
        }
    } else {
        responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `StepType key is missing.`);
    }
}

/**
 * [Search Agency/ BrandList]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchAgencyList = (req, res) => {

    var searchAgencyPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/searchagency`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchAgencyPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
    }
    /**
     * [Search Advertiser List]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */
const searchAdvertiserList = (req, res) => {

    var searchListPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/searchadvertiser`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchListPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
    }
    /*
     * [Search Brand  List]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */
const searchBrandList = (req, res) => {

    var searchBrandPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/searchbrand`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchBrandPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
    }
    /*
     * [Campaign Details By Id]
     * @param  {[type]} req [object received from the application.]
     * @param  {[type]} res [object to be sent as response from the api.]
     * @return {[type]}     [function call to return the appropriate response]
     */
const campaignDetails = (req, res) => {

    var campDetailsPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/campaigndetails`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campDetailsPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/*
 * [CSV Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const uploadCSV = async(req, res) => {
    const multiparty = require('multiparty');
    const csvData = new multiparty.Form();
    // console.log(csvData);
    csvData.parse(req, function(err, fields, files) {
        // console.log(fields);
        var csvFilePath = files.file[0].path;
        csv()
            .fromFile(csvFilePath)
            .then((jsonObj) => {
                // console.log(jsonObj);
                var dataArray = {
                    "admin_id": fields.admin_id,
                    "platform": fields.platform,
                    "data": jsonObj
                }
                const options = {
                    // headers: { 'campaign_id': campaignId },
                    uri: campaignbaseUrl + `/campaign/upload/csv`,
                    json: dataArray,
                    resolveWithFullResponse: true,
                    method: 'POST'
                };
                return request(options).then((response) => {
                    if (response.body) {
                        res.send(response.body);
                    } else {
                        res.send({ 'responseCode': 404, 'responseMessage': 'error' });
                    }
                }).catch((err) => {
                    res.send({ 'responseCode': 500, 'responseMessage': 'error' });
                })
            });

    });

}

/*
 * [CSV Upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const premiumUploadCSV = async(req, res) => {
    const multiparty = require('multiparty');
    const csvData = new multiparty.Form();
    // console.log(csvData);
    csvData.parse(req, function(err, fields, files) {
        // console.log(fields);
        var csvFilePath = files.file[0].path;
        csv()
            .fromFile(csvFilePath)
            .then((jsonObj) => {
                // console.log(jsonObj);
                var dataArray = {
                    "admin_id": fields.admin_id,
                    "platform": fields.platform,
                    "data": jsonObj
                }
                const options = {
                    // headers: { 'campaign_id': campaignId },
                    uri: campaignbaseUrl + `/campaign/premium/csv/upload`,
                    json: dataArray,
                    resolveWithFullResponse: true,
                    method: 'POST'
                };
                return request(options).then((response) => {
                    if (response.body) {
                        res.send(response.body);
                    } else {
                        res.send({ 'responseCode': 404, 'responseMessage': 'error' });
                    }
                }).catch((err) => {
                    res.send({ 'responseCode': 500, 'responseMessage': 'error' });
                })
            });

    });

}

/*
 * [Campaign Details By Id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignDetailsId = (req, res) => {
    var campIdPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/campaigndetails`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campIdPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/*
 * [Search Micro Influencers]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const searchMicroInfluencer = (req, res) => {
    var searchMicroPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/searchinfluencers`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    searchMicroPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/*
 * [Search Micro Influencers]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getCampaignList = (req, res) => {
    var getCampaignPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/campaignlist`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    getCampaignPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign image delete]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignImageDelete = (req, res) => {

    var imageDeletePromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/image/delete`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    imageDeletePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign influencer remove]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignInfluencerRemove = (req, res) => {
    var influencerRemovePromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/influencer/remove`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    influencerRemovePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign influencer platform handle remove]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignInfluencerHandleRemove = (req, res) => {

    var handleRemovePromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/influencer/handle/remove`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    handleRemovePromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}


const campaignStop = (req, res) => {

    var campStopPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/campaignstopstatus`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campStopPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Campaign influencer remove]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const campaignKolsList = (req, res) => {
    var campListPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/kol/campaignKolsList`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    campListPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/* kol campaign platform listing */
const campaignPlatformListing = (req, res) => {
    var platformPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/kol/campaignplatform`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    platformPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
    }

    /*search kol micro influencer */
const campaignSearchMicro = (req, res) => {
    var microPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/kol/microSearch`,
            json: true,
            resolveWithFullResponse: true,
            // headers: { 'user_id': req.headers['user_id'] },
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    microPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}


/* kol add  influencer */
const campaignAddInfluencer = (req, res) => {

    var addInfluencerPromise = new Promise(function(resolve, reject) {

        const options = {
            uri: campaignbaseUrl + `/campaign/kol/addmicroinfluencer`,
            json: true,
            resolveWithFullResponse: true,
            json: req.body,
            method: 'POST'
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }

            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })

    addInfluencerPromise.then(response => {
        res.send(response);
    })
    .catch(e => {
        res.send(helper.errorResponse(e));
    })
}

/**
 * [Single image/video upload]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const uploadImage = (req, res) => {
        const multiparty = require('multiparty');
        const formImage = new multiparty.Form();
        formImage.parse(req, async(err, fields, files) => {
            if (fields.status == '1') {
                if (files.imageObject && files.imageObject[0] != '') {
                    await helper.uploadFiles('image/', files.imageObject[0].path, files.imageObject[0].originalFilename, (error1, success) => {
                        res.send({ 'responseCode': 200, 'responseMessage': 'image upload successfully', "imageLink": success });
                    });
                } else {
                    res.send({ 'responseCode': 404, 'responseMessage': 'image not upload successfully', "imageLink": '' });
                }
            } else {
                if (files.videoObject && files.videoObject[0] != '') {
                    await helper.uploadVideoFile('video/', files.videoObject[0].path, files.videoObject[0].originalFilename, (error1, success) => {
                        res.send({ 'responseCode': 200, 'responseMessage': 'video upload successfully', "videoLink": success });
                    });
                } else {
                    res.send({ 'responseCode': 404, 'responseMessage': 'video not upload successfully', "videoLink": '' });
                }
            }

        });
    }

const adminResetPassword = (req, res) => {
    req.body.decoded = req.decoded;
    var adminResetPromise = new Promise(function(resolve, reject) {

        const options = {
            method: 'POST',
            uri: userbaseUrl + `/api/admin/reset-password`,
            json: req.body,
            resolveWithFullResponse: true,
        };
        return request(options).then((response) => {
                if (response.body.responseCode == 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            })
            .catch(e => {
                res.send(helper.errorResponse(e));
            })
    })
    adminResetPromise.then(response => {
            res.send(response);
        })
        .catch(e => {
            res.send(helper.errorResponse(e));
        })
}

    /*Export apis*/
module.exports = {
    advertiserlist,
    advertiserDetails,
    searchOrganisation,
    organisationlist,
    addOrganisation,
    updateOrganisation,
    updateLocationOrganisation,
    getOrganisation,
    getAdminList,
    adminLogin,
    adminCreate,
    adminStatusUpdate,
    adminUpdate,
    adminPasswordUpdate,
    adminUserData,
    adminImageUpload,
    adminChangePassword,
    adminUserList,
    create_campaign,
    campaignDiscard,
    campaignCompleted,
    campaignDelete,
    campaignInfluencers,
    campaignPlatformDel,
    campaignEditStatus,
    camapignManagerList,
    searchAgencyList,
    searchAdvertiserList,
    searchBrandList,
    campaignDetails,
    uploadCSV,
    premiumUploadCSV,
    campaignDetailsId,
    searchMicroInfluencer,
    getCampaignList,
    campaignImageDelete,
    campaignInfluencerRemove,
    campaignInfluencerHandleRemove,
    campaignStop,
    campaignPlatformListing,
    campaignSearchMicro,
    campaignAddInfluencer,
    campaignKolsList,
    uploadImage,
    adminResetPassword
}