const campaignController = require("../controllers/campaignController");
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();
// Get all Advertisers
router.post('/advertisers', checkToken, campaignController.advertiserlist);
router.post('/advertisers-list', checkToken, campaignController.advertiserlist);
router.post('/get-advertiser', campaignController.advertiserDetails);
// Organisation Search
router.post('/admin/search-organisation', checkToken, campaignController.searchOrganisation);
// Get all organisation
router.post('/organisations', checkToken, campaignController.organisationlist);
router.post('/organizations-list', checkToken, campaignController.organisationlist);
//
// router.post('/organisation/address', campaignController.organisationAddress);
// Add organisation
router.post('/admin/add-organisation', checkToken, campaignController.addOrganisation);
router.post('/admin/update-organisation', checkToken, campaignController.updateOrganisation);
router.post('/admin/location-organisation', checkToken, campaignController.updateLocationOrganisation);
router.post('/admin/get-organisation', checkToken, campaignController.getOrganisation);
// admin List
router.post('/adminlist', checkToken, campaignController.getAdminList);
// admin Login
router.post('/adminlogin', campaignController.adminLogin);
// admin create
router.post('/admin/create', checkToken, campaignController.adminCreate);
// admin status update
router.post('/admin/status_update', checkToken, campaignController.adminStatusUpdate);
// admin update
router.post('/admin/update', checkToken, campaignController.adminUpdate);
// admin Password Update
router.post('/admin/password_update', checkToken, campaignController.adminPasswordUpdate);
//get admin user data
router.get('/admin/user_data', checkToken, campaignController.adminUserData);
// admin image upload
router.post('/admin/user_image_upload', checkToken, campaignController.adminImageUpload);
// admin password change
router.post('/admin/change_password', checkToken, campaignController.adminChangePassword);
// users List
router.get('/userlist', checkToken, campaignController.adminUserList);

//reset password for superadmin
router.post('/admin/reset-password', checkToken, campaignController.adminResetPassword)

/********************Campaign Apis ********************/
//Create campaign api with step wise
router.post('/create_campaign', checkToken, campaignController.create_campaign);
// Campaigns
router.post('/discard', checkToken, campaignController.campaignDiscard);
router.post('/completed', checkToken, campaignController.campaignCompleted);
router.post('/delete', checkToken, campaignController.campaignDelete);
router.post('/influencers', checkToken, campaignController.campaignInfluencers);
router.post('/platform/delete', checkToken, campaignController.campaignPlatformDel);
router.post('/edit/status', checkToken, campaignController.campaignEditStatus);
router.post('/upload/csv', checkToken, campaignController.uploadCSV);
router.post('/premium/csv/upload', checkToken, campaignController.premiumUploadCSV);
router.post('/image/delete', checkToken, campaignController.campaignImageDelete);

// get camapign manager list
router.post('/getmanagerlist', checkToken, campaignController.camapignManagerList);
// search agency or brand in create campaign
router.post('/searchagencylist', checkToken, campaignController.searchAgencyList);
// search advertiser of agency or brand
router.post('/searchadvertiserlist', checkToken, campaignController.searchAdvertiserList);
// search brand
router.post('/searchbrandlist', checkToken, campaignController.searchBrandList);
// get campaign detail by id
router.post('/details', checkToken, campaignController.campaignDetailsId);
//get filters of micro influencer
router.post('/searchmicroinfluencer', checkToken, campaignController.searchMicroInfluencer);
//campaign list
router.post('/allcampaignlist', checkToken, campaignController.getCampaignList);
//campaignstop
router.post('/campaignstop', checkToken, campaignController.campaignStop);
// manage kol campaign platform listing
router.post('/kol/campaignplatformlisting', checkToken, campaignController.campaignPlatformListing);
// manage kol search micro influencer
router.post('/kol/searchmicro', checkToken, campaignController.campaignSearchMicro);
// manage kol add influencer
router.post('/kol/addinfluencer', checkToken, campaignController.campaignAddInfluencer);



///////////////////////////////////////Campaign Manage List KOL APis/////////////////////////////////////////////////////////
router.post('/influencer/remove', checkToken, campaignController.campaignInfluencerRemove);
router.post('/influencer/handle/remove', checkToken, campaignController.campaignInfluencerHandleRemove);
//Campaign manage kol list
router.post('/kols_list',checkToken, campaignController.campaignKolsList);
//single Image upload api
router.post('/uploadImage', checkToken,campaignController.uploadImage);

/*Export*/
module.exports = router;