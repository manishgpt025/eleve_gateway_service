# Eleve Global - Gateway Service

Node version 10.X.

## Development server

Clone from repository `git clone https://{bitbucket_user_name}@bitbucket.org/elevetech/eleve-global.git`

Run `npm i` for package dependencies.

Run `nodemon` for a dev server. 

Navigate to `http://localhost:8000/`.

## Note

After this you need to run the other micro services as `user_services, ideation_services and campaign_services` to communicate with the system.

