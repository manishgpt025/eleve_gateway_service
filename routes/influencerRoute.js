// Packages

// Controllers
const influencerController = require("../controllers/influencerController");
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();
/******************** Admin Panel Influencer Management********************/
// Create new influencer
router.post('/create_influencer', checkToken, influencerController.createInfluencer);
// check email in influencer
router.post('/check_email', checkToken, influencerController.checkEmailExist);
// edit detail in influencer
router.post('/edit_detail', checkToken, influencerController.editDetailInfluencer);
// get detail by influencer id
router.post('/get_influencer', checkToken, influencerController.getInfluencerDetail);
router.post('/status/update', checkToken, influencerController.statusUpdate);
router.post('/get_download_list', checkToken, influencerController.getInfluencerListDataForExcel);
router.post('/get_list', checkToken, influencerController.getInfluencerList);
// edit social detail in influencer
router.post('/edit_social_detail', checkToken, influencerController.editSocialDetailInfluencer);
// delete social detail in influencer
router.post('/delete_social_card', checkToken, influencerController.deleteSocialCard);
// genre List in influencer
router.post('/genreList', checkToken, influencerController.genreList);
//check handle url of social accounts
router.post('/checkHandleUrl', checkToken, influencerController.checkHandleUrl);
//get all social platforms of influencer
router.post('/get_platforms', checkToken, influencerController.getPlatforms);
// Get max reach count for influencer
router.get('/get_max_reach_count',checkToken, influencerController.get_max_reach_count);
/******************** Influencer Campaign Management ********************/
router.post('/campaign/new',checkToken, influencerController.campaignNew);
router.post('/campaign/detail',checkToken, influencerController.campaignDetail);
router.post('/campaign/completed',checkToken, influencerController.campaignCompleted);
router.post('/campaign/action', checkToken, influencerController.campaignAction);
// Influencer Campaign Twitter Post
router.post('/campaign/twitter/post', checkToken, influencerController.campaignTwitterPost);
router.post('/campaign/influencerauthentication', influencerController.checkAuthentication);
router.post('/campaign/retweetupdate', influencerController.retweetUpdateCount);
/******************** Influencer Campaign Management Ends ********************/

module.exports = router;