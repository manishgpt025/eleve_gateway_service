const ideationController = require("../controllers/ideationController");
const router = require('express').Router();
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// detail route
router.post('/boarddetail', checkToken, ideationController.boardDetail);
// board Status
router.post('/boardstatus', checkToken,  ideationController.boardStatus);
// 
router.get('/boards/:id/:type?/:filter?/:sort?', checkToken, ideationController.InfluencerBoardsList);
// board Contents Added
router.post('/boardcontent', checkToken, ideationController.boardContent);
// board LivePosts Added
router.post('/boardliveposts', checkToken, ideationController.boardLiveposts);
// Live URL Delete
router.post('/remove/live/url', checkToken, ideationController.removeLiveUrl)

// Live URL Delete
router.post('/getmetadata/url', checkToken, ideationController.getMetaData);


module.exports = router;