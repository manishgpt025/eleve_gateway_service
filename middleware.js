let jwt = require('jsonwebtoken');
const config = require('./globalConfig.js');
const secret = `${global.gConfig.secret}`;

const responseHandle = require('./globalFunctions/responseHandle.js');
const responseCode = require('./globalFunctions/httpResponseCode.js');
const responseMessage = require('./globalFunctions/httpResponseMessage.js');

let checkToken = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    if (token && token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }

    if (token) {
        jwt.verify(token, secret, (err, decoded) => {
            if (err) {
                return responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, err);
            } else {
                req.decoded = decoded;
                req.body.decoded = decoded;
                next();
            }
        });
    } else {
        return responseHandle.sendResponseWithData(res, responseCode.UNAUTHORIZED, 'Auth token is not supplied');
    }
};


module.exports = {
    checkToken: checkToken
}