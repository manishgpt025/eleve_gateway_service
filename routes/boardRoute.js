const boardCtrl = require("../controllers/boardController");
const multer = require('multer');
const multerS3 = require('multer-s3');
const AWS = require('aws-sdk');
const aws_config = AWS.config.loadFromPath('./s3_config.json');
const s3Config = new AWS.S3(aws_config);
var multiparty = require('multiparty');
var http = require('http');
var util = require('util');
const path = require('path');
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();

router.route('/health/check').get(boardCtrl.healthCheck);
router.route('/updatereport').post(checkToken, boardCtrl.updateReport);
router.route('/removeboardmember').post(checkToken, boardCtrl.removeBoardMember);
router.route('/removeboardattachment').post(checkToken, boardCtrl.removeBoardAttachment);
router.route('/list').get(checkToken, boardCtrl.boardList);
router.route('/boardcolleague').post(checkToken, boardCtrl.boardColleagues);
router.route('/boardcontentsubmit').post(checkToken, boardCtrl.submitContentBoard);
router.route('/boardcontentconfirm').post(checkToken, boardCtrl.boardContentConfirm);
router.route('/boardcontentlist').post(checkToken,boardCtrl.boardContentList);
router.route('/influencer/search').post(checkToken, boardCtrl.boardContentSearchInfluencer);
router.route('/conversationoveriew').post(checkToken, boardCtrl.getOverviewData);
router.route('/boardconversationlist').post(checkToken, boardCtrl.getOverviewList);
router.route('/search/influencerprofile').post(checkToken, boardCtrl.getSearchInfluencer);
router.route('/addinfluencer').post(checkToken, boardCtrl.addInfluencerTab);
router.route('/update/status').post(checkToken, boardCtrl.updateStatus);
router.route('/invite/influencer').post(checkToken, boardCtrl.inviteInfluencer);
router.route('/board/platforms').post(checkToken, boardCtrl.getboardPlatforms);
router.route('/boardstatuslist').post(checkToken, boardCtrl.statusList);
router.route('/boardestimatedlist').post(checkToken, boardCtrl.estimatedList);
router.route('/multipleinvite').post(checkToken, boardCtrl.multipleInviteInfluencer);
router.route('/search/brand').post(checkToken, boardCtrl.searchBrand);
router.route('/search/brand/colleagues').post(checkToken, boardCtrl.searchBrandColleagues);
/**Influencer Collaboration Routes */
router.route('/influencer/submitcontent').post( checkToken, boardCtrl.influencerContentSubmit);
router.route('/influencer/contentlist').post( checkToken,boardCtrl.influencerContentList);
router.route('/influencer/selectedprofiles').post( checkToken,boardCtrl.getSelectedProfiles);
router.route('/influencer/recentupdate').post( checkToken, boardCtrl.getRecentUpdates);
router.route('/influencer/boardlist').post( checkToken, boardCtrl.getBoardList);
router.route('/influencer/recentupdate').post( checkToken, boardCtrl.getRecentUpdates);
router.route('/start')
    /** GET /boards/start - Start board */
    .get(checkToken, boardCtrl.startBoard)

router.route('/create')
    /** PUT /boards/create - Create board */
    .post(checkToken,boardCtrl.updateBoard)


router.route('/:boardId')
    /** GET /boards/:boardId - Get board */
    .get(checkToken, boardCtrl.getBoard)

router.route('/:boardId')
    /** DELETE /boards/:boardId -Delete board */
    .patch(checkToken, boardCtrl.deleteBoard)

router.route('/uploadfiles')
    /** POST /boards/uploadfiles -  Upload Files */
    .post(boardCtrl.uploadFiles.array('file', 10), function(req, res, next) {
        const files = req.files;
        if (!files) {
            const error = new Error('Please choose files')
            error.httpStatusCode = 400
            return next(error)
        }
        res.send(req.files);
    })

router.route("/upload")
    .post(checkToken, function(req, res) {

        var upload = multer({
            storage: multerS3({
                s3: s3Config,
                bucket: process.env.S3_BUCKET_NAME,
                metadata: function(req, file, cb) {
                    cb(null, { fieldName: file.fieldname });
                },
                acl: 'public-read',
                key: function(req, file, cb) {
                    let flag = req.body.flag;
                    if (flag == 1 || flag == 2) {
                        if (req.files.length > 1) {
                            res.send({ responseCode: 400, responseMessage: "Maximum 1 file is allowed", result: [] });
                            return false;
                        }
                    } else if (flag == 3) {
                        if (req.files.length > 10) {
                            res.send({ responseCode: 400, responseMessage: "Maximum 10 files are allowed", result: [] });
                            return false;
                        }
                    } else {
                        res.send({ responseCode: 400, responseMessage: "Flag is not passed", result: [] });
                        return false;
                    }
                    var newFileName = Date.now() + "-" + file.originalname;
                    var fullPath = 'collaboration_file/' + newFileName;
                    cb(null, fullPath);
                }
            }),
            fileFilter: function(req, file, callback) {
                let ext = file.mimetype;
                let flag = req.body.flag;
                if (ext !== 'application/pdf' && flag == 2) {
                    res.send({ responseCode: 400, responseMessage: "Only pdf file are allowed.", result: [] });
                    return false;
                }
                callback(null, true)
            }
        }).any();

        upload(req, res, function(err) {
            const files = req.files;
            let flag = req.body.flag;
            let board_id = req.body.board_id;

            if (files.length == 0) {
                res.send({ responseCode: 400, responseMessage: "Please choose file", result: [] });
            } else {

                let files_data = [];
                files.forEach(element => {
                    file_key = {};
                    file_key.key = element.key;
                    files_data.push(file_key);

                });

                if(flag==1){
                    req.file_path = files_data[0].key;
                    req.board_id = board_id;
                    req.upload_ro = 1;
                    return boardCtrl.saveRo(req, res)
                    .then(upload_success=>{
                        if(upload_success){
                            return res.send({ 'responseCode': 200, 'responseMessage': "File uploaded successfully.", result: files_data });
                        }else{
                            return res.send({ 'responseCode': 400, 'responseMessage': "File not uploaded.", result: files_data });
                        }
                    })

                }

                return res.send({ 'responseCode': 200, 'responseMessage': "File uploaded successfully.", result: files_data });
            }


        });



    })

// upload board contents by csv
router.route('/uploadBoardContents')
    .post(checkToken, boardCtrl.uploadBoardContents);

/** GET /boards/econtracts    - Get Influencers Contracts */
router.route('/influencer/econtracts')
    /** GET /boards/start - Start board */
    .get(checkToken, boardCtrl.getEcontracts)

/** GET /boards/econtracts    - Get Influencers Contracts */
router.route('/upload/econtracts')
    /** GET /boards/start - Start board */
    .get(boardCtrl.getEcontracts)


// API route for adding social profile in board_influencer table    
router.route('/addSocialProfile')
    .post(checkToken, boardCtrl.addSocial)

// POST /boards/econtracts - Get Latest econtracts of influencer on board
router.route('/influencer/latestEcontract')
    .post(checkToken, boardCtrl.getLatestEcontract);

//POST /boards/saveEcontracts - POST saveEcontracts in board_influencer table
router.route('/influencer/saveEcontracts')
    .post(checkToken, boardCtrl.saveEcontracts)

//POST /boards/removeKol - Remove influencer from board        
router.route('/influencer/removeKol')
    .post(checkToken, boardCtrl.removeKol)

//POST /boards/removeSocial - Remove influencer from board        
router.route('/influencer/removesocial')
    .post(checkToken, boardCtrl.removeSocial)


//POST /boards/addNotes  - Add notes for the influencer in Influencer Tab
router.route('/influencer/addNotes')
    .post(checkToken, boardCtrl.addNotes);

//POST /boards/addBrief - Add brief for the influencer in Influencer Tab
router.route('/influencer/addBrief')
    .post(checkToken, boardCtrl.addBrief);

/** GET /boards/influencer/list    - Get Influencers List With Filter */
router.route('/influencer/list')
    /** GET /boards/influencer/list  - Influencer Board List  */
    .get(checkToken, boardCtrl.getBoardInfluencers)

router.route('/influencer/rejectBrief')
    /** POST /boards/influencer/rejectBrief - */
    .post(checkToken, boardCtrl.rejectBrief);

/* get board details of a influencer*/
router.route('/influencer/get-board-details')
    /** POST /boards/influencer/rejectBrief - */
    .get( checkToken, boardCtrl.getBoardDetails);

/* get breif details for a influencer in board */
router.route('/influencer/getbriefdetails')
    /** POST /boards/influencer/rejectBrief - */
    .post(checkToken, boardCtrl.getBriefDetails);

/* For saving actions (accept and reject )of an influencer in a board*/
router.route('/influencer/save-action')
    /** POST /boards/influencer/action - */
    .post( checkToken, boardCtrl.saveInfluencerAction);



/*Export*/
module.exports = router;