const express = require('express')
const app = express();
require('dotenv').config();
const config = require('./globalConfig.js');
const bodyParser = require('body-parser');
const server = require('http').createServer(app);
cors = require('cors');
const userRoute = require('./routes/userRoute.js');
const ideationRoute = require('./routes/ideationRoute.js');
const campaignRoute = require('./routes/campaignRoute.js');
const influencerRoute = require('./routes/influencerRoute.js');
const brandRoute = require('./routes/brandRoute.js');
const bloggerRoute = require('./routes/bloggerRoute.js');
const boardRoute = require('./routes/boardRoute.js');
const contentRoute = require('./routes/contentRoute.js');

app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.json({ limit: '100mb', extended: true }));
app.use(cors());

//call route by api
app.use('/api', userRoute);
app.use('/ideation', ideationRoute);
app.use('/campaign', campaignRoute);
app.use('/influencer', influencerRoute);
app.use('/brand', brandRoute);
app.use('/blogger', bloggerRoute);
app.use('/boards', boardRoute);
app.use('/content', contentRoute);

//connectivity forn listen
server.listen(global.gConfig.node_port, () => {
    console.log('Gateway service listening on port', global.gConfig.node_port)
});