module.exports = {
    development: {
        node_port: process.env.GATEWAY_PORT,
        url: process.env.SERVICE_HOST,
        secret: process.env.SECRET_KEY,
        user_service_port: process.env.USER_SERVICE_PORT,
        ideation_service_port: process.env.IDEATION_SERVICE_PORT,
        base_url: 'http://' + process.env.USER_SERVICE_HOST + ':' + process.env.USER_SERVICE_PORT,
        ideation_base_url: 'http://' + process.env.IDEATION_SERVICE_HOST + ':' + process.env.IDEATION_SERVICE_PORT,
        campaign_base_url: 'http://' + process.env.CAMPAIGN_SERVICE_HOST + ':' + process.env.CAMPAIGN_SERVICE_PORT,
        brand_base_url: 'http://' + process.env.BRAND_SERVICE_HOST + ':' + process.env.BRAND_SERVICE_PORT,
        board_base_url: 'http://' + process.env.COLLABORATION_SERVICE_HOST + ':' + process.env.COLLABORATION_SERVICE_PORT,
    },
    stagging: {
        node_port: process.env.DEV_GATEWAY_PORT,
        url: process.env.DEV_SERVICE_HOST,
        secret: process.env.SECRET_KEY,
        user_service_port: process.env.DEV_USER_SERVICE_PORT,
        ideation_service_port: process.env.DEV_IDEATION_SERVICE_PORT,
        base_url: 'http://' + process.env.DEV_SERVICE_HOST + ':' + process.env.DEV_USER_SERVICE_PORT,
        ideation_base_url: 'http://' + process.env.DEV_SERVICE_HOST + ':' + process.env.DEV_IDEATION_SERVICE_PORT,
        campaign_base_url: 'http://' + process.env.DEV_SERVICE_HOST + ':' + process.env.DEV_CAMPAIGN_SERVICE_PORT,
        brand_base_url: 'http://' + process.env.DEV_SERVICE_HOST + ':' + process.env.DEV_BRAND_SERVICE_PORT,
        board_base_url: 'http://' + process.env.DEV_SERVICE_HOST + ':' + process.env.DEV_COLLABORATION_SERVICE_PORT,
    }
};