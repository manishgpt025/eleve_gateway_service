const request = require('request-promise');
const blogger_base_url = global.gConfig.blogger_base_url;
//use helper
const helper = require('../globalFunctions/function.js');
let jwt = require('jsonwebtoken');
const secret = `${global.gConfig.secret}`;
/**
 * [login brand of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getBloggerList = (req, res) => {
    const options = {
        method: 'POST',
        uri: blogger_base_url + `/blogger/list`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body.responseCode == 200) {
            res.send(response.body);
        } else {
            res.send(response.body);
        }
    }).catch((err) => {
        console.log('error_status_code:' + err.statusCode)
    })
}

// Export
module.exports = {
    getBloggerList: getBloggerList
}