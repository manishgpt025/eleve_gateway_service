const brandController = require("../controllers/brandController");
// Middleware
const middleware = require('../middleware.js');
let checkToken = middleware.checkToken;
// Router
const router = require('express').Router();

// Login
router.post('/brandlogin', brandController.brandLogin);

/*Export*/
module.exports = router;