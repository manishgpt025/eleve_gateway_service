const request = require('request-promise');
const baseUrl = global.gConfig.ideation_base_url;
//use helper
const helper = require('../globalFunctions/function.js');
const multiparty = require('multiparty');
const fs = require('fs');
/**
 * [Board Detail of a particular board of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const boardDetail = (req, res) => {
    const options = {
        method: 'POST',
        uri: baseUrl + `/ideation/detail`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


/**
 * [Board status Update of a particular board of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const boardStatus = (req, res) => {
    const options = {
        method: 'POST',
        uri: baseUrl + `/ideation/boardstatus`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Board influencer by id]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const InfluencerBoardsList = (req, res) => {
    const options = {
        method: 'GET',
        uri: baseUrl + `/ideation/boards/` + req.params.id + `/` + req.params.type + `/` + req.params.filter + `/` + req.params.sort,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [Board content]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const boardContent = (req, res) => {
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) {
        console.log("fields ---------------", fields);
        console.log("files ---------------", files);
        //return;
        if (files.image) {
            fs.readFile(files.image[0].path, function(err, image) {
                if (err) { throw err; } else {
                    helper.uploadFile(fields.user_id, image, files.image[0].originalFilename, (error3, imageUpload) => {
                        if (error3) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error3);
                        } else {
                            let data = {
                                image: imageUpload,
                                text: fields.text,
                                platform: fields.platform,
                                status: fields.status,
                                date: fields.date,
                                user_id: fields.user_id,
                                board_id: fields.board_id
                            }
                            const options = {
                                method: 'POST',
                                uri: baseUrl + `/ideation/boardcontent`,
                                json: data,
                                resolveWithFullResponse: true,

                            };
                            return request(options).then((response) => {

                                res.send(response.body);

                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })
                        }
                    });
                }
            });
        } else {
            let data = {
                text: fields.text,
                platform: fields.platform,
                status: fields.status,
                date: fields.date,
                user_id: fields.user_id,
                board_id: fields.board_id
            }
            const options = {
                method: 'POST',
                uri: baseUrl + `/ideation/boardcontent`,
                json: data,
                resolveWithFullResponse: true,

            };
            return request(options).then((response) => {

                res.send(response.body);

            }).catch((err) => {
                console.log(err);
                console.log('errorstatuscode:' + err.statusCode)
            })
        }
        // res.send(fields);
    });

    // const options = {
    //     method: 'POST',
    //     uri: baseUrl + `/ideation/boardcontent`,
    //     json: req.body,
    //     resolveWithFullResponse: true,

    // };
    // return request(options).then((response) => {

    //     res.send(response.body);

    // }).catch((err) => {
    //     console.log(err);
    //     console.log('errorstatuscode:' + err.statusCode)
    // })
}

/**
 * [Board Contents Update/Added of a particular board of an user]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */


/**
 * [board Live posts]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const boardLiveposts = (req, res) => {
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) {
        console.log('files--------------------', files)
        console.log('field--------------------', fields)
        if (files.image) {
            fs.readFile(files.image[0].path, function(err, image) {
                if (err) { throw err; } else {
                    helper.uploadFile(fields.user_id, image, files.image[0].originalFilename, (error3, imageUpload) => {
                        if (error3) {
                            responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, error3);
                        } else {
                            let data = {
                                image: imageUpload,
                                text: fields.text,
                                platform: fields.platform,
                                date: fields.date,
                                user_id: fields.user_id,
                                board_id: fields.board_id,
                                description: fields.description,
                                title: fields.title
                            }
                            const options = {
                                method: 'POST',
                                uri: baseUrl + `/ideation/boardliveposts`,
                                json: data,
                                resolveWithFullResponse: true,

                            };
                            return request(options).then((response) => {

                                res.send(response.body);

                            }).catch((err) => {
                                console.log(err);
                                console.log('errorstatuscode:' + err.statusCode)
                            })
                        }
                    });
                }
            });
        } else {
            let data = {
                text: fields.text,
                platform: fields.platform,
                date: fields.date,
                user_id: fields.user_id,
                board_id: fields.board_id,
                description: fields.description,
                title: fields.title
            }
            const options = {
                method: 'POST',
                uri: baseUrl + `/ideation/boardliveposts`,
                json: data,
                resolveWithFullResponse: true,

            };
            return request(options).then((response) => {

                res.send(response.body);

            }).catch((err) => {
                console.log(err);
                console.log('errorstatuscode:' + err.statusCode)
            })
        }
    });


}


// const boardLiveposts = (req, res) => {
//     console.log(req.body);
//     //return;
//     const options = {
//         method: 'POST',
//         uri: baseUrl + `/ideation/boardliveposts`,
//         // json: req.body,
//         resolveWithFullResponse: true,

//     };
//     return request(options).then((response) => {

//         res.send(response.body);

//     }).catch((err) => {
//         console.log(err);
//         console.log('errorstatuscode:' + err.statusCode)
//     })
// }

/**
 * [Board Remove Live URL]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function caboardLivepostsll to return the appropriate response]
 */
const removeLiveUrl = (req, res) => {

    const options = {
        method: 'POST',
        uri: baseUrl + `/ideation/remove/live/url`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}

/**
 * [get Meta Data]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getMetaData = (req, res) => {

    const options = {
        method: 'POST',
        uri: baseUrl + `/ideation/getmetadata/url`,
        json: req.body,
        resolveWithFullResponse: true,

    };
    return request(options).then((response) => {
        if (response.body) {
            res.send(response.body);
        } else {
            res.send({ 'responseCode': 404, 'responseMessage': 'error' });
        }
    }).catch((err) => {
        res.send({ 'responseCode': 500, 'responseMessage': 'error' });
    })
}


// Export
module.exports = {
    boardDetail: boardDetail,
    boardStatus: boardStatus,
    InfluencerBoardsList: InfluencerBoardsList,
    boardContent: boardContent,
    boardLiveposts: boardLiveposts,
    removeLiveUrl: removeLiveUrl,
    getMetaData: getMetaData
}